//
//  WhatsNewViewController.swift
//  RCAC84
//
//  Created by Ch on 15/4/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import Kingfisher

protocol JoinEventDelegate {
    func join(eventId:String)
}

class WhatsNewViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    @IBOutlet weak var eventCollectionView: UICollectionView!
    @IBOutlet weak var AOMH: NSLayoutConstraint!
    @IBOutlet weak var evenCollectionH: NSLayoutConstraint!
    
    var artoftheMonth = Array<Artwork>()
    var currentEvents = Array<Event>()
    var storedEventId = ""
    let services = Services.sharedInstance
    

    override func viewDidLoad() {
        super.viewDidLoad()
        eventCollectionView.register(UINib.init(nibName: "EventCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "eventCell")
        galleryCollectionView.register(UINib.init(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "gridCell")
        
        services.getCurrentEvents(delegate: self)
        services.getArtoftheMonth(delegate: self)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        services.getAccount(delegate: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == galleryCollectionView) {
            let h = collectionView.frame.size.width
            self.AOMH.constant = CGFloat(h)
            return artoftheMonth.count
        } else {
            return currentEvents.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        if(collectionView == galleryCollectionView) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gridCell", for: indexPath as IndexPath) as! ImageCollectionViewCell
            let url = URL(string: artoftheMonth[indexPath.row].image)!
            cell.image.kf.setImage(with: url)

            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "eventCell", for: indexPath as IndexPath) as! EventCollectionViewCell
            let dataAtIndexPath = currentEvents[indexPath.row]
            
            if dataAtIndexPath.mediaList.count > 0 {
                let url = URL(string: dataAtIndexPath.mediaList[0].url)!
                cell.poster.kf.setImage(with: url)
                if (dataAtIndexPath.mediaList.count > 1) {
                    //TODO: open media lightbox
                }
            }
            cell.eventName.text = dataAtIndexPath.name
            cell.eventDetail.text = dataAtIndexPath.detail
            cell.eventDate.text = dataAtIndexPath.eventDate
            cell.eventId = dataAtIndexPath.id
            cell.delegate = self
//            if dataAtIndexPath.agendas.count > 0 {
            cell.setAgendas(eventDate: dataAtIndexPath.dateList)
//            }
            if dataAtIndexPath.tags.count > 0 {
                cell.setTage(tags: dataAtIndexPath.tags)
            }
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (collectionView == eventCollectionView) {
            
            var width  = self.view.frame.size.width;
            if width > 800 {
                width = 700
            }
            evenCollectionH.constant = width * 0.9
    
            if currentEvents.count <= 1 {
                return CGSize.init(width: width * 0.95, height: width * 0.85)
            } else {
                return CGSize.init(width: width * 0.9, height: width * 0.85)
            }
        }
        
        return CGSize(width: collectionView.frame.size.width/3, height: collectionView.frame.size.width/3)
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        if collectionView == eventCollectionView {
            let vc = EventDetailViewController()
            vc.eventId = currentEvents[indexPath.row].id
            print("You selected event #\(currentEvents[indexPath.row].id)!")
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = ArtworkViewController()
            vc.artworkId = artoftheMonth[indexPath.row].id
            print("You selected artwork #\(artoftheMonth[indexPath.row].id)!")
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
        
    }

}

extension WhatsNewViewController:ServicesDelegete {
    func callback(from: String, response: Any, tag: String) {
        if tag == Services.GETCURRENTEVENTS {
            self.currentEvents = response as! Array<Event>
            self.eventCollectionView.reloadData()
        } else if tag == Services.GETARTOFTHEMONTH {
            self.artoftheMonth = response as! Array<Artwork>
            self.galleryCollectionView.reloadData()
        } else if tag == Services.JOINEVENT {
            print("JOIN EVEN: \(response)")
            
            let alert = UIAlertController(title: "Complete!", message: "Successfully join event", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            alert.addAction(UIAlertAction(title: "See Event", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    let vc = EventDetailViewController()
                    let res = response as! JoinResponse
                    vc.eventId = res.eventId
                    self.navigationController?.pushViewController(vc, animated: true)
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            
            self.present(alert, animated: true, completion: nil)
            
        } else if tag == Services.JOINEVENTFAIL {
            
            let alert = UIAlertController(title: "Fail!", message: "You're already register this event.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            alert.addAction(UIAlertAction(title: "See Event", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    let vc = EventDetailViewController()
//                    let res = response as! JoinResponse
                    if self.storedEventId != "" {
                        vc.eventId = self.storedEventId
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension WhatsNewViewController:JoinEventDelegate {
    
    func join(eventId:String) {
        if User.sharedInstance.id != nil && User.sharedInstance.id != "" {
            if eventId != "" {
                storedEventId = eventId
            }
            services.joinEvent(eventId: eventId, delegate: self)
        } else {
            let alert = UIAlertController(title: "Warning", message: "Please signin before register the event and try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style {
                case .default:
                    print("default")
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            self.present(alert, animated: true, completion: nil)
        }
    }

}
