//
//  SearchViewController.swift
//  RCAC84
//
//  Created by Ch on 25/4/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    // MARK: - Properties
    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchBar: UITextField!
    
    var services = Services.sharedInstance
    
    var searchObj = Array<SearchObj>()
    
//    let searchController = UISearchController(searchResultsController: nil)
    
    // MARK: - View Setup
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        tableView.register(UINib.init(nibName: "SearchTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchCell")
        tableView.register(UINib.init(nibName: "PreviousEventTableViewCell", bundle: nil), forCellReuseIdentifier: "PVEvent")
        
        self.searchBar.layer.cornerRadius = 5.0
        self.searchBar.layer.masksToBounds = true
        self.searchBar.delegate = self
        
        self.searchBar.setLeftPaddingPoints(10)
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called \(textField.text!) \(string) \(range.location) \(range.length)")
        
        if range.length == 0 {
            services.search(q: textField.text! + string, delegate: self)
        } else {
            let q = (textField.text)?.prefix(((textField.text?.count)!-1))
            services.search(q: String(q!), delegate: self)
        }
        
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        services.search(q: textField.text!, delegate: self)
        textField.resignFirstResponder()
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return searchObj.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchObj[section].sectionName == SearchObj.SectionName.EVENTS {
            return searchObj[section].sectionObjects.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return searchObj[section].sectionName.rawValue
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if searchObj[indexPath.section].sectionName == SearchObj.SectionName.EVENTS {
            var events = searchObj[indexPath.section].sectionObjects as! Array<Event>
//        if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PVEvent", for: indexPath) as! PreviousEventTableViewCell
            cell.date.text = events[indexPath.row].dateString
            cell.name.text = events[indexPath.row].name
            cell.detail.text = events[indexPath.row].detail
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! SearchTableViewCell
            cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
            if searchObj[indexPath.section].sectionName == SearchObj.SectionName.ARTWORKS {
                cell.galleryCollectionView.tag = 11
            } else if searchObj[indexPath.section].sectionName == SearchObj.SectionName.ARTISTS {
                cell.galleryCollectionView.tag = 22
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if searchObj[section].sectionObjects.count <= 0 {
            return 0
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if searchObj[indexPath.section].sectionObjects.count <= 0 {
            return 0
        } else {
            if searchObj[indexPath.section].sectionName == SearchObj.SectionName.EVENTS {
                return 74
            }
            let x = Int(tableView.frame.size.width/4) * Int((searchObj[indexPath.section].sectionObjects.count/4) + 1)
            return CGFloat.init(x)
        }
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if searchObj[indexPath.section].sectionName == SearchObj.SectionName.EVENTS {
//            return 74
//        }
//        return tableView.frame.size.width/2
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("did selected tableView cell")
        
        if indexPath.section == 2 {
            let vc = EventDetailViewController()
            let event = searchObj[2].sectionObjects[indexPath.row] as! Event
            vc.eventId = event.id
            print("You selected event #\(event.id)!")
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension SearchViewController: ServicesDelegete {
    func callback(from: String, response: Any, tag: String) {

        if tag == Services.SEARCH {
            let res = response as! Array<SearchObj>
            
            for r in res {
                r.toString()
            }
            if res.count > 0 {
                searchObj = res
            }
            searchObj = res
            tableView.reloadData()
        }
    }
    
}

extension SearchViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("did selected collectionview cell")
        
        if collectionView.tag == 11 {
            let vc = ArtworkViewController()
            vc.artworkId = (searchObj[0].sectionObjects[indexPath.row] as! Artwork).id
//            print("You selected artwork #\(artoftheMonth[indexPath.row].id)!")
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if collectionView.tag == 22 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ArtistViewController") as! ArtistViewController
            vc.artistId = (searchObj[1].sectionObjects[indexPath.row] as! Artist).id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 11 {
            print("numberOfItemsInSection \(0):\(searchObj[0].sectionName) \(searchObj[0].sectionObjects.count)")
            return searchObj[0].sectionObjects.count
        } else if collectionView.tag == 22 {
            print("numberOfItemsInSection \(1):\(searchObj[1].sectionName) \(searchObj[1].sectionObjects.count)")
            return searchObj[1].sectionObjects.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gridCell", for: indexPath as IndexPath) as! ImageCollectionViewCell

        if collectionView.tag == 11 { //ARTWORK
            let url = URL(string: (searchObj[0].sectionObjects[indexPath.row] as! Artwork).image)!
            cell.image.kf.setImage(with: url)
        } else if collectionView.tag == 22 { //ARTIST
            let url = URL(string: (searchObj[1].sectionObjects[indexPath.row] as! Artist).profilePic)!
            cell.image.kf.setImage(with: url)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width/4, height: collectionView.frame.size.width/4)
    }
    
}
