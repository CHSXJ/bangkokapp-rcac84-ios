//
//  GalleryViewController.swift
//  RCAC84
//
//  Created by Ch on 15/4/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import TagListView

class GalleryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tagListView: TagListView!
    var selectedTagId = Array<String>()
    var tagNameList = Array<String>()
    var allTag = Array<Tag>()
    @IBOutlet weak var tagScrollView: UIScrollView!
    
    var footerView:CustomFooterView?
    let footerViewReuseIdentifier = "RefreshFooterView"
    var isLoading:Bool = false

    var services = Services.sharedInstance
    
    let reuseIdentifier = "gridCell"
    var items = Array<Gallery>()
    var allItem = Array<Gallery>()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        services.getAllTags(delegate: self)
        services.getGalleryByTags(ids: [], delegate: self)
        collectionView.register(UINib.init(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "gridCell")
        collectionView.register(UINib(nibName: "CustomFooterView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: footerViewReuseIdentifier)
        tagListView.delegate = self
        tagListView.textFont = UIFont.systemFont(ofSize: 15)
        tagListView.alignment = .left // possible values are .Left, .Center, and .Right
        tagScrollView.layer.cornerRadius = 8.0
        tagScrollView.clipsToBounds = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! ImageCollectionViewCell
        
        let url = URL(string: self.items[indexPath.item].image)!
        cell.image.kf.setImage(with: url)
        cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isLoading {
            return CGSize.zero
        }
        return CGSize(width: collectionView.frame.size.width/3, height: collectionView.frame.size.width/3)
    }
    
    @objc func collectionView(_ collectionView: UICollectionView, layout  collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize{
        let size = CGSize(width: collectionView.frame.size.width, height: 40)
        return size
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = ArtworkViewController()
        vc.artworkId = items[indexPath.row].id
        print("You selected artwork #\(items[indexPath.row].id)!")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionFooter {
            let aFooterView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath) as! CustomFooterView
            self.footerView = aFooterView
            self.footerView?.backgroundColor = UIColor.clear
            return aFooterView
        } else {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath)
            return headerView
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionElementKindSectionFooter {
            self.footerView?.prepareInitialAnimation()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionElementKindSectionFooter {
            self.footerView?.stopAnimate()
        }
    }
    
    //compute the scroll value and play witht the threshold to get desired effect
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let threshold   = 100.0 ;
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        var triggerThreshold  = Float((diffHeight - frameHeight))/Float(threshold);
        triggerThreshold   =  min(triggerThreshold, 0.0)
        let pullRatio  = min(fabs(triggerThreshold),1.0);
        self.footerView?.setTransform(inTransform: CGAffineTransform.identity, scaleFactor: CGFloat(pullRatio))
        if pullRatio >= 0.3 {
            self.footerView?.animateFinal()
        }
        print("pullRation:\(pullRatio)")
    }
    
    //compute the offset and call the load method
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        let pullHeight  = fabs(diffHeight - frameHeight);
        print("pullHeight:\(pullHeight)");
        if pullHeight >= 0.0 {
            print("footerview:\(String(describing: self.footerView?.isAnimatingFinal))");
            if (self.footerView?.isAnimatingFinal)! {
                print("load more trigger")
                self.isLoading = true
                self.footerView?.startAnimate()
                if #available(iOS 10.0, *) {
                    Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer:Timer) in
                        for i:Int in self.items.count + 0...self.items.count + 14 {
                            if self.allItem.count > i {
                                self.items.append(self.allItem[i])
                            }
                        }
                        self.collectionView.reloadData()
                        self.isLoading = false
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }

}

extension GalleryViewController:TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        
        tagView.isSelected = !tagView.isSelected
        
        selectedTagId.removeAll()
//        if self.tagListView.selectedTags().count > 0 {
            for tagName in self.tagListView.selectedTags() {
                let index = tagNameList.index(of: tagName.currentTitle!)
                selectedTagId.append(allTag[index!].id)
            }
            print("selected cell: \(selectedTagId)")
            services.getGalleryByTags(ids: selectedTagId, delegate: self)
//        } else {
//            items.removeAll()
//            collectionView.reloadDataWithTransition()
//        }
    }
}

extension GalleryViewController:ServicesDelegete {
    
    func callback(from: String, response: Any, tag: String) {
        
        if tag == Services.GETALLTAGS {
            print("ALL TAGS \(response)")
            allTag = response as! Array<Tag>
            for tag in allTag {
                tagListView.addTag(tag.name)
                tagNameList.append(tag.name)
            }
        } else if tag == Services.GETGALLERYBYTAGS {
            allItem = response as! Array<Gallery>
            items.removeAll()
            for i:Int in 0...14 {
                if allItem.count > 15 {
                    items.append(allItem[i])
                }
            }
            collectionView.reloadDataWithTransition()
        }
    }
}

extension UICollectionView {
    func reloadDataWithTransition() {
        UIView.transition(with: self,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: { self.reloadData() })
    }
}

