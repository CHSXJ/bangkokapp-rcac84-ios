//
//  SearchTableViewCell.swift
//  RCAC84
//
//  Created by Ch on 15/7/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        galleryCollectionView.register(UINib.init(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "gridCell")
        galleryCollectionView.reloadData()
        // Initialization code
    }
    
    func setCollectionViewDataSourceDelegate
        <D: UICollectionViewDataSource & UICollectionViewDelegate>
        (dataSourceDelegate: D, forRow row: Int) {
        
        galleryCollectionView.delegate = dataSourceDelegate
        galleryCollectionView.dataSource = dataSourceDelegate
        galleryCollectionView.tag = row
        galleryCollectionView.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
