//
//  EventsViewController.swift
//  RCAC84
//
//  Created by Ch on 19/4/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import SwiftyJSON

class EventsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let services = Services.sharedInstance
    @IBOutlet weak var eventCollectionView: UICollectionView!
    
    var currents = Array<Event>()
    var nexts = Array<Event>()
    var storedEventId = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        eventCollectionView.register(UINib.init(nibName: "EventCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "eventCell")
        let XIB = UINib.init(nibName: "SectionHeader", bundle: Bundle.main)
        eventCollectionView.register(XIB, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "Header")
        services.getCurrentEvents(delegate: self)
        services.getNextEvents(delegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return currents.count
        } else {
            return nexts.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "eventCell", for: indexPath as IndexPath) as! EventCollectionViewCell
        var dataAtIndexPath:Event
        if indexPath.section == 0 {
            dataAtIndexPath = self.currents[indexPath.row];
        } else {
            dataAtIndexPath = self.nexts[indexPath.row];
        }
        
        if dataAtIndexPath.mediaList.count > 0 {
            let url = URL(string: dataAtIndexPath.mediaList[0].url)!
            cell.poster.kf.setImage(with: url)
            if (dataAtIndexPath.mediaList.count > 1) {
                //TODO: open media lightbox
            }
        }
        cell.eventName.text = dataAtIndexPath.name
        cell.eventDetail.text = dataAtIndexPath.detail
        cell.eventDate.text = dataAtIndexPath.eventDate
        cell.eventId = dataAtIndexPath.id
        cell.setAgendas(eventDate: dataAtIndexPath.dateList)
        cell.delegate = self
        
        if dataAtIndexPath.tags.count > 0 {
            cell.setTage(tags: dataAtIndexPath.tags)
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var width = self.view.frame.size.width;
        if width > 800 {
            width = 700
        }
        return CGSize.init(width: width * 0.95, height: width * 0.85)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        // Dequeue Reusable Supplementary View
        if let supplementaryView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "Header", for: indexPath) as? SectionHeader {
            // Configure Supplementary View
            supplementaryView.backgroundColor = UIColor.init(white: 1.0, alpha: 0.0)
            if indexPath.section == 0 {
                supplementaryView.titleLabel.text = "CURRENT EVENTS"
            } else {
                supplementaryView.titleLabel.text = "NEXT EVENTS"
            }
            
            return supplementaryView
        }
        
        fatalError("Unable to Dequeue Reusable Supplementary View")
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = EventDetailViewController()
        if indexPath.section == 0 {
            vc.eventId = currents[indexPath.row].id
            
        } else {
            vc.eventId = nexts[indexPath.row].id
        }
        
        print("You selected event #\(vc.event.id)!")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize.init(width: collectionView.frame.size.width, height: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: 50.0)
    }

}

extension EventsViewController:ServicesDelegete {
    func callback(from: String, response:Any, tag: String) {
        if tag == Services.GETCURRENTEVENTS {
            self.currents = response as! Array<Event>
            self.eventCollectionView.reloadData()
        } else if tag == Services.GETNEXTEVENTS {
            self.nexts = response as! Array<Event>
            self.eventCollectionView.reloadData()
        } else if tag == Services.JOINEVENT {
            print("JOIN EVEN: \(response)")
    
            let alert = UIAlertController(title: "Complete!", message: "Successfully join event", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
    
                case .cancel:
                    print("cancel")
    
                case .destructive:
                    print("destructive")
                }}))
            alert.addAction(UIAlertAction(title: "See Event", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    let vc = EventDetailViewController()
                    let res = response as! JoinResponse
                    vc.eventId = res.eventId
                    self.navigationController?.pushViewController(vc, animated: true)
                case .cancel:
                    print("cancel")
    
                case .destructive:
                    print("destructive")
                }}))
    
            self.present(alert, animated: true, completion: nil)
    
        } else if tag == Services.JOINEVENTFAIL {
    
            let alert = UIAlertController(title: "Fail!", message: "You're already register this event.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
    
                case .cancel:
                    print("cancel")
    
                case .destructive:
                    print("destructive")
                }}))
            alert.addAction(UIAlertAction(title: "See Event", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    let vc = EventDetailViewController()
                    if self.storedEventId != "" {
                        vc.eventId = self.storedEventId
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                case .cancel:
                    print("cancel")
    
                case .destructive:
                    print("destructive")
                }}))
    
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension EventsViewController:JoinEventDelegate {
    
    func join(eventId:String) {
        if User.sharedInstance.id != nil && User.sharedInstance.id != "" {
            if eventId != "" {
                storedEventId = eventId
            }
            services.joinEvent(eventId: eventId, delegate: self)
        } else {
            let alert = UIAlertController(title: "Warning", message: "Please signin before register the event and try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
}



