//
//  CustomFooterView.swift
//  CustomRefreshControl
//
//  Created by Ch on 06/01/19.



import Foundation
import UIKit
import NVActivityIndicatorView

class CustomFooterView : UICollectionReusableView {
   
//    @IBOutlet weak var refreshControlIndicator: NVActivityIndicatorView!
    var isAnimatingFinal:Bool = false
    var currentTransform:CGAffineTransform?
    
    var refreshControlIndicator = NVActivityIndicatorView.init(frame: CGRect.init(x: 0, y: 0, width:60, height:60), type: NVActivityIndicatorType.ballBeat, color: UIColor.white, padding:15)

    override func awakeFromNib() {
        super.awakeFromNib()
        self.prepareInitialAnimation()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func setTransform(inTransform:CGAffineTransform, scaleFactor:CGFloat) {
        if isAnimatingFinal {
            return
        }
        self.currentTransform = inTransform
        self.refreshControlIndicator.transform = CGAffineTransform.init(scaleX: scaleFactor, y: scaleFactor)
    }
    
    //reset the animation
    func prepareInitialAnimation() {
        self.isAnimatingFinal = false
        self.refreshControlIndicator.stopAnimating()
        self.refreshControlIndicator.transform = CGAffineTransform.init(scaleX: 0.0, y: 0.0)
        self.refreshControlIndicator.color = UIColor(red:0.98, green:0.85, blue:0.39, alpha:1.00)
        self.refreshControlIndicator.center = CGPoint.init(x: self.frame.width/2, y: self.frame.height/2)
        self.addSubview(self.refreshControlIndicator)
        
//        loading.backgroundColor = UIColor(red:0.98, green:0.85, blue:0.39, alpha:1.00)
//        loading.layer.cornerRadius = 5
//        loading.center = CGPoint.init(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2)
//        UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(loading)
//        self.loading.startAnimating()
    }
    
    func startAnimate() {
        self.isAnimatingFinal = true
        self.refreshControlIndicator.startAnimating()
    }
    
    func stopAnimate() {
        self.isAnimatingFinal = false
        self.refreshControlIndicator.stopAnimating()
    }
    
    //final animation to display loading
    func animateFinal() {
        if isAnimatingFinal {
            return
        }
        self.isAnimatingFinal = true
        UIView.animate(withDuration: 0.2) { 
            self.refreshControlIndicator.transform = CGAffineTransform.identity
        }
    }
}
