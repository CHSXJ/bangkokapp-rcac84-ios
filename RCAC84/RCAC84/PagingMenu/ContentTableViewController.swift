//
//  ContentTableViewController.swift
//  RCAC84
//
//  Created by Ch on 14/4/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit

class ContentTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!

    var data: [(emoji: String, name: String)] = [
        (emoji: "🐶", name: "Dog"),
        (emoji: "🐱", name: "Cat"),
        (emoji: "🦁", name: "Lion"),
        (emoji: "🐴", name: "Horse"),
        (emoji: "🐮", name: "Cow"),
        (emoji: "🐷", name: "Pig"),
        (emoji: "🐭", name: "Mouse"),
        (emoji: "🐹", name: "Hamster"),
        (emoji: "🐰", name: "Rabbit"),
        (emoji: "🐻", name: "Bear"),
        (emoji: "🐨", name: "Koala"),
        (emoji: "🐼", name: "Panda"),
        (emoji: "🐔", name: "Chicken"),
        (emoji: "🐤", name: "Baby"),
        (emoji: "🐵", name: "Monkey"),
        (emoji: "🦊", name: "Fox"),
        (emoji: "🐸", name: "Frog"),
        (emoji: "🦀", name: "Crab"),
        (emoji: "🦑", name: "Squid"),
        (emoji: "🐙", name: "Octopus"),
        (emoji: "🐬", name: "Dolphin"),
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! ContentTableViewCell
        cell.configure(data: data[indexPath.row])
        return cell
    }


    @available(iOS 11.0, *)
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        guard let tableViewLayoutMargin = tableViewLayoutMargin else { return }
        
        tableView.layoutMargins = tableViewLayoutMargin
    }

    /// To support safe area, all tableViews aligned on scrollView (superview) needs to be set margin for the cell's contentView and separator.
    @available(iOS 11.0, *)
    private var tableViewLayoutMargin: UIEdgeInsets? {
        guard let superview = view.superview else {
            return nil
        }
        
        let defaultTableContentInsetLeft: CGFloat = 16
        return UIEdgeInsets(
            top: 0,
            left: superview.safeAreaInsets.left + defaultTableContentInsetLeft,
            bottom: 0,
            right: 0
        )
    }
}
