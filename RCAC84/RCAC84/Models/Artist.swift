//
//  Artist.swift
//  RCAC84
//
//  Created by Ch on 19/6/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import SwiftyJSON

class Artist: NSObject {

    var id:String = ""
    var nameth:String = ""
    var nameen:String = ""
    var awards:String = ""
    var experience:String = ""
    var education:String = ""
    var story:String = ""
    var nationality:String = ""
    var email:String = ""
    var mobile:String = ""
    var profilePic:String = ""
    
    func makeEventObj(data:JSON) -> Artist {
        
        if data["id"].string != nil {
            self.id = data["id"].string!
        } else {
            self.id = ""
        }
//        self.id = data["id"].string!
        self.nameth = data["nameth"].string!
        self.nameen = data["nameen"].string!
        self.awards = data["awards"].string!
        self.experience = data["experience"].string!
        self.education = data["education"].string!
        self.story = data["story"].string!
        self.nationality = data["nationality"].string!
        self.email = data["email"].string!
        self.mobile = data["mobile"].string!
        self.profilePic = data["profilePic"].string!
        
        return self
    }
}
