//
//  Event.swift
//  RCAC84
//
//  Created by Ch on 26/5/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import SwiftyJSON

class Event: NSObject {
    
    enum EventType:String {
        case SCHEDULED = "SCHEDULED"
        case DRAFT = "DRAFT"
    }
    var id:String = ""
    var mediaList:Array<Media> = []
    var name:String = ""
    var startDateStr:String = ""
    var endDateStr:String = ""
    var museumId:String = ""
    var status:EventType = EventType.DRAFT
    var location:String = ""
    var detail:String = ""
//    var agendas:Array<Agenda> = []
    var dateList = Array<EventDate>()
    var dateString:String = ""
    
    var maxAttendee:Int = 0
    var eventDate:String = ""
    var sampleArtists:Array<Sample> = []
    var sampleArtworks:Array<Sample> = []
    var tags:Array<Tag> = []
    
    class Sample {
        var id:String = ""
        var imageURL:String = ""
        
        func makeSample(data:JSON) -> Sample{
            self.id = data["id"].string!
            self.imageURL = data["imageURL"].string!
            
            return self
        }
    }
    
    func makeEventObj(data:JSON) -> Event {
        if data["name"] != JSON.null {
            self.name = data["name"].string!
        }
        if data["id"] != JSON.null {
            self.id = data["id"].string!
        }
        if data["about"] != JSON.null {
            self.detail = data["about"].string!
        }
        if data["dateList"].count > 0 {
            for d in data["dateList"] {
                self.dateList.append( EventDate.init().makeEventDate(data: d.1))
            }
        }
        if data["sampleArtists"].count > 0 {
            for artist in data["sampleArtists"]{
                self.sampleArtists.append( Sample.init().makeSample(data: artist.1) )
            }
        }
        if data["sampleArtworks"].count > 0 {
            for artwork in data["sampleArtworks"]{
                self.sampleArtworks.append( Sample.init().makeSample(data: artwork.1) )
            }
        }
        if data["tags"].count > 0 {
            for tag in data["tags"]{
                self.tags.append( Tag.init().setTag(data: tag.1) )
            }
        }
//        self.museumId = data["museumId"].string!
        if data["location"] != JSON.null {
            self.location = data["location"].string!
        }
        //        if data["status"].string! == EventType.DRAFT.rawValue {
        //            self.status = EventType.DRAFT
        //        }
        
        // TODO: Medialist & DateList
        if data["mediaList"].count > 0 {
            for m in data["mediaList"] {
                self.mediaList.append( Media.init().makeMediaObj(data: m.1) )
            }
        }
//        print("mediaList : \(self.mediaList)")
        
//        let newFormatter = DateFormatter()
//        newFormatter.dateFormat = "dd MMM"//this your string date format
//        let startDate = Formatter.iso8601.date(from: data["startDate"].string!)
//        let endDate = Formatter.iso8601.date(from: data["endDate"].string!)
//        self.eventDate = "\(newFormatter.string(from: startDate!)) - \(newFormatter.string(from: endDate!))"
        
        return self
    }
    
}

//extension Formatter {
//    @available(iOS 11.0, *)
//    static let iso8601: ISO8601DateFormatter = {
//        let formatter = ISO8601DateFormatter()
//        formatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
//        return formatter
//    }()
//}

extension Formatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        return formatter
    }()
}
extension Date {
    var iso8601: String {
        return Formatter.iso8601.string(from: self)
    }
}

extension String {
    var dateFromISO8601: Date? {
        return Formatter.iso8601.date(from: self)   // "Mar 22, 2017, 10:22 AM"
    }
}
