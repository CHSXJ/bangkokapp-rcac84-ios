//
//  Artwork.swift
//  RCAC84
//
//  Created by Ch on 4/6/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import SwiftyJSON

class Artwork: NSObject {
    
    var id:String = ""
    var artistId:String = ""
    var name:String = ""
    var concept:String = ""
    var technique:String = ""
    var size:String = ""
    var year:String = ""
    var image:String = ""
    var artOfMonth:Bool = false
    var artist = Artist()
    var events = Array<RelatedEvent>()
    var tags:Array<Tag> = []


    func makeEventObj(data:JSON) -> Artwork {
        
        self.id = data["id"].string!
        self.artistId = data["artistId"].string!
        self.name = data["name"].string!
        self.concept = data["concept"].string!
        self.technique = data["technique"].string!
        self.size = data["size"].string!
//        try self.year = (data["year"].string)!
        self.image = data["image"].string!
        self.artOfMonth = data["artOfMonth"].bool!
        
        if data["artist"] != JSON.null {
            self.artist = Artist().makeEventObj(data: data["artist"])
        }
        if data["events"].count > 0 {
            for e in data["events"]{
                self.events.append( RelatedEvent.init().makeEventObj(data: e.1))
            }
        }
        if data["tags"].count > 0 {
            for tag in data["tags"]{
                self.tags.append( Tag.init().setTag(data: tag.1) )
            }
        }
        
        return self
    }
    
}
