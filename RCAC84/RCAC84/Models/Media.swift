//
//  Media.swift
//  RCAC84
//
//  Created by Ch on 11/7/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import SwiftyJSON

class Media: NSObject {
    
    static let IMAGE = "IMAGE"
    static let VIDEO = "VIDEO"
    static let YOUTUBE = "YOUTUBE"
    
    var contentType:String = ""
    var url:String = ""
    var type:String = ""
    var thumbUrl:String = ""
    
    func makeMediaObj(data:JSON) -> Media {
        
        if data["contentType"] != JSON.null {
            self.contentType = data["contentType"].string!
        }
        if data["url"] != JSON.null {
            self.url = data["url"].string!
        }
        if data["thumbUrl"] != JSON.null {
            self.thumbUrl = data["thumbUrl"].string!
        }
        if data["type"] != JSON.null {
            if data["type"].stringValue == Media.IMAGE {
                self.type = Media.IMAGE
            } else if data["type"].stringValue == Media.VIDEO {
                self.type = Media.VIDEO
            } else if data["type"].stringValue == Media.YOUTUBE {
                self.type = Media.YOUTUBE
            } else {
                self.type = Media.IMAGE
            }
        }
        
        return self
    }
    
    public func toString() {
        
        print("content type: \(self.contentType)")
        print("url: \(self.url)")
        print("type: \(self.type)")
        print("thumbUrl: \(self.thumbUrl)")
        
    }

}
