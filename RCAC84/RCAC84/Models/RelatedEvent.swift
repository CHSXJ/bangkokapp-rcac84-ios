//
//  RelatedEvent.swift
//  RCAC84
//
//  Created by Ch on 13/7/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import SwiftyJSON

class RelatedEvent: NSObject {
    
    var id:String = ""
    var dateString:String = ""
    var name:String = ""
    var location:String = ""
    var about:String = ""
    
    func makeEventObj(data:JSON) -> RelatedEvent {
        
        if data["id"] != JSON.null {
            self.id = data["id"].string!
        }
        if data["dateString"] != JSON.null {
            self.dateString = data["dateString"].string!
        }
        if data["name"] != JSON.null {
            self.name = data["name"].string!
        }
        if data["location"] != JSON.null {
            self.location = data["location"].string!
        }
        if data["about"] != JSON.null {
            self.about = data["about"].string!
        }
        
        return self
    }

}
