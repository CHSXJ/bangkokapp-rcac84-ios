//
//  News.swift
//  RCAC84
//
//  Created by Ch on 1/7/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import SwiftyJSON

class News: NSObject {
    
    var id:String = ""
    var name:String = ""
    var date:String = ""
    var text:String = ""
    var image:String = ""
    
//    "name": "Winter is coming.",
//    "date": "2018-07-01",
//    "text": "Solumsaperet gloriatur adolescens inceptos.  Persiusdui latine solum massa est suspendisse est delicata appareat quam voluptatum sapientem.  \nMontesreque dolorum nisi phasellus vitae cursus nobis duis vocibus habeo novum graeci pharetra euripidis nostrum consectetuer.  Rutrumsuavitate cu latine possim nonumes definitiones parturient quaestio viverra vim.  Populomi reprehendunt cras cum postea viderer ludus erat sagittis.  Aequeoption deseruisse malorum tellus conceptam docendi equidem debet.  Suasalia tincidunt.  Monteslibero iriure corrumpit ridiculus noluisse.",
//    "images": [
//    "https://picsum.photos/400/400?x=436549",
//    "https://picsum.photos/400/400?x=368161",
//    "https://picsum.photos/400/400?x=041911"
//    ]

    func makeNewObj(data:JSON) -> News {
        
        self.id = data["id"].string!
        self.name = data["name"].string!
        self.text = data["text"].string!
        self.image = data["image"].string!
        
        return self
    }
}
