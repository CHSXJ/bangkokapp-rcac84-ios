//
//  Tag.swift
//  RCAC84
//
//  Created by Ch on 5/6/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import SwiftyJSON

class Tag: NSObject {
    
    var id:String = ""
    var name:String = ""
    var nameen:String = ""
    var nameth:String = ""
    var links = Array<String>()
    var lastUpdate:String = ""
    
    func setTag(data:JSON) -> Tag {
        
        self.id = data["id"].string!
        
        if data["name"] != JSON.null {
            self.name = data["name"].string!
        }
        if data["nameen"] != JSON.null {
            self.nameen = data["nameen"].string!
        }
        if data["nameth"] != JSON.null {
            self.nameth = data["nameth"].string!
        }
        if data["lastUpdate"] != JSON.null {
            self.lastUpdate = "\(String(describing: Formatter.iso8601.date(from: data["lastUpdate"].string!)))"
        }
        
        return self
    }
}
