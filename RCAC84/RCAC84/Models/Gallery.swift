//
//  Gallery.swift
//  RCAC84
//
//  Created by Ch on 1/7/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import SwiftyJSON

class Gallery: NSObject {
    var id:String = ""
    var image:String = ""
    
    func makeEventObj(data:JSON) -> Gallery {
        
        self.id = data["id"].string!
        self.image = data["image"].string!
    
        return self
    }
}
