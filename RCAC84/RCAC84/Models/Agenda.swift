//
//  Agenda.swift
//  RCAC84
//
//  Created by Ch on 23/6/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import SwiftyJSON

class Agenda: NSObject {
    
    var time:String = ""
    var name:String = ""
    var detail:String = ""

    func makeEventObj(data:JSON) -> Agenda {
        
        if data["time"] != JSON.null {
            self.time = data["time"].string!
        }
        if data["name"] != JSON.null {
            self.name = data["name"].string!
        }
        if data["description"] != JSON.null {
            self.detail = data["description"].string!
        }
        
        return self
    }
}
