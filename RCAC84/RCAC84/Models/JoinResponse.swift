//
//  JoinResponse.swift
//  RCAC84
//
//  Created by Ch on 24/7/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import SwiftyJSON

class JoinResponse: NSObject {
    
    var id:String = ""
    var name:String = ""
    var dateString:String = ""
    var email:String = ""
    var firstName:String = ""
    var lastName:String = ""
    var age:String = ""
    var userId:String = ""
    var eventId:String = ""
    var gender:String = ""
    var phone:String = ""
    var about:String = ""
    
    func makeObj(data:JSON) -> JoinResponse {
        
        if data["id"] != JSON.null {
            self.id = data["id"].string!
        }
        if data["name"] != JSON.null {
            self.name = data["name"].string!
        }
        if data["dateString"] != JSON.null {
            self.dateString = data["dateString"].string!
        }
        if data["email"] != JSON.null {
            self.email = data["email"].string!
        }
        if data["firstName"] != JSON.null {
            self.firstName = data["firstName"].string!
        }
        if data["lastName"] != JSON.null {
            self.lastName = data["lastName"].string!
        }
        if data["age"] != JSON.null {
            self.age = data["age"].string!
        }
        if data["userId"] != JSON.null {
            self.userId = data["userId"].string!
        }
        if data["eventId"] != JSON.null {
            self.eventId = data["eventId"].string!
        }
        if data["gender"] != JSON.null {
            self.gender = data["gender"].string!
        }
        if data["phone"] != JSON.null {
            self.phone = data["phone"].string!
        }
        if data["about"] != JSON.null {
            self.about = data["about"].string!
        }
        
        return self
    }

}
