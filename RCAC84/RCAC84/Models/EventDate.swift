//
//  EventDate.swift
//  RCAC84
//
//  Created by Ch on 12/7/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import SwiftyJSON

class EventDate: NSObject {

    var agendas = Array<Agenda>()
    var instant = ""
    var dateStr:String = ""
    
    func makeEventDate(data:JSON) -> EventDate {
        
        if data["agendas"].count > 0 {
            for agenda in data["agendas"]{
                self.agendas.append( Agenda.init().makeEventObj(data: agenda.1))
            }
        }
        let newFormatter = DateFormatter()
        newFormatter.dateFormat = "dd MMM"//this your string date format
        let dDay = Formatter.iso8601.date(from: data["instant"].string!)
        self.instant = data["instant"].string!
        if data["dateStr"] != JSON.null {
            self.dateStr = data["dateStr"].string!
        }
        
        
        //        let newFormatter = DateFormatter()
        //        newFormatter.dateFormat = "dd MMM"//this your string date format
        //        let startDate = Formatter.iso8601.date(from: data["startDate"].string!)
        //        let endDate = Formatter.iso8601.date(from: data["endDate"].string!)
        //        self.eventDate = "\(newFormatter.string(from: startDate!)) - \(newFormatter.string(from: endDate!))"
        return self
    }
    
}
