//
//  SearchObj.swift
//  RCAC84
//
//  Created by Ch on 18/7/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import SwiftyJSON

class SearchObj: NSObject {
    
    enum SectionName: String {
        case ARTISTS, ARTWORKS, EVENTS, ANY
    }
    var sectionName : SectionName = SectionName.ANY
    var sectionObjects = NSMutableArray()
    
    func toString() {
        print("Section Name: \(self.sectionName))")
        for n in sectionObjects {
            print("OBJ: \(n)")
        }
    }

}
