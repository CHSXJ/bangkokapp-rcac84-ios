//
//  User.swift
//  RCAC84
//
//  Created by Ch on 16/7/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import SwiftyJSON

class User: NSObject {
    
    static let sharedInstance = User()
    
    var id : String?
    var login : String?
    var firstName : String?
    var lastName : String?
    var email : String?
    var imageUrl : String?
    var activated : Bool?
    var langKey : String?
    var createdBy : String?
    var createdDate : String?
    var lastModifiedBy : String?
    var lastModifiedDate : String?
    var authorities : [String]?
    var title : String?
    var birthDate : String?
    var nationality : String?
        
    func makeUserObj(data:JSON) -> User {
        
        self.id = data["id"].string
        
        if data["login"] != JSON.null {
            self.login = data["login"].string
        } else {
            self.login = ""
        }
        
        if data["firstName"] != JSON.null {
            self.firstName = data["firstName"].string
        } else {
            self.firstName = ""
        }
        
        if data["lastName"] != JSON.null {
            self.lastName = data["lastName"].string
        } else {
            self.lastName = ""
        }
        
        if data["email"] != JSON.null {
            self.email = data["email"].string
        } else {
            self.email = ""
        }
        
        if data["imageUrl"] != JSON.null {
            self.imageUrl = data["imageUrl"].string
        } else {
            self.imageUrl = ""
        }
        
        if data["activated"] != JSON.null {
            self.activated =  data["activated"].bool
        } else {
            self.activated =  false
        }
        
        if data["langKey"] != JSON.null {
            self.langKey = data["langKey"].string
        } else {
            self.langKey = ""
        }
        
        if data["createdBy"] != JSON.null {
            self.createdBy = data["createdBy"].string
        } else {
            self.createdBy = ""
        }
        
        if data["createdDate"] != JSON.null {
            self.createdDate = data["createdDate"].string
        } else {
            self.createdDate = ""
        }
        
        if data["lastModifiedBy"] != JSON.null {
            self.lastModifiedBy = data["lastModifiedBy"].string
        } else {
            self.lastModifiedBy = ""
        }
        
        if data["lastModifiedDate"] != JSON.null {
            self.lastModifiedDate = data["lastModifiedDate"].string
        } else {
            self.lastModifiedDate = ""
        }
        
        if data["title"] != JSON.null {
            self.title = data["title"].string
        } else {
            self.title = ""
        }
        
        if data["birthDate"] != JSON.null {
            self.birthDate = data["birthDate"].string
        } else {
            self.birthDate = ""
        }
        
        if data["nationality"] != JSON.null {
            self.nationality = data["nationality"].string
        } else {
            self.nationality = ""
        }
        
        return self
    }
    

}
