//
//  Museum.swift
//  RCAC84
//
//  Created by Ch on 20/9/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import SwiftyJSON

class Museum: NSObject {
    
    var id:String = ""
    var aboutUrl:String = ""
    var address:String = ""
    var country:String = ""
    var email:String = ""
    var lat:Float = 0
    var lon:Float = 0
    var name:String = ""
    var openingHours:String = ""
    var phoneNumber:String = ""
    var privacyPolicyUrl:String = ""
    var shortName:String = ""
    var termOfServiceUrl:String = ""
    var mapURL:String = ""
    
    func makeMuseumObj(data:JSON) -> Museum {
        
        if data["id"] != JSON.null {
            self.id = data["id"].string!
        }
        if data["aboutUrl"] != JSON.null {
            self.aboutUrl = data["aboutUrl"].string!
        }
        if data["address"] != JSON.null {
            self.address = data["address"].string!
        }
        if data["country"] != JSON.null {
            self.country = data["country"].string!
        }
        if data["email"] != JSON.null {
            self.email = data["email"].string!
        }
        if data["lat"] != JSON.null {
            self.lat = data["lat"].floatValue
        }
        if data["lon"] != JSON.null {
            self.lon = data["lon"].floatValue
        }
        if data["name"] != JSON.null {
            self.name = data["name"].string!
        }
        if data["openingHours"] != JSON.null {
            self.openingHours = data["openingHours"].string!
        }
        if data["phoneNumber"] != JSON.null {
            self.phoneNumber = data["phoneNumber"].string!
        }
        if data["privacyPolicyUrl"] != JSON.null {
            self.privacyPolicyUrl = data["privacyPolicyUrl"].string!
        }
        if data["shortName"] != JSON.null {
            self.shortName = data["shortName"].string!
        }
        if data["termOfServiceUrl"] != JSON.null {
            self.termOfServiceUrl = data["termOfServiceUrl"].string!
        }
        if data["mapURL"] != JSON.null {
            self.mapURL = data["mapURL"].string!
        }
        
        return self
    }
    
}
