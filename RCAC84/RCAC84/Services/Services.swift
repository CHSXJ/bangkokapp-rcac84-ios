//
//  Services.swift
//  RCAC84
//
//  Created by Ch on 20/5/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import SwiftyJSON

protocol ServicesDelegete {
    func callback(from:String, response:Any, tag:String)
}

class MashapeHeadersadapter: RequestAdapter {
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        
        urlRequest.setValue("Bearer \(UserDefaults.standard.string(forKey: "h") ??  "")", forHTTPHeaderField: "Authorization")
        urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        return urlRequest
    }
}

class Services: NSObject {
    
    static let sharedInstance = Services()
    static let SIGNIN = "Sign in"
    static let AUTHENSUCCESS = "Authen Success"
    static let AUTHENFAIL = "Authen Fail"
    static let SIGNOUT = "Sign Out"
    static let GETCURRENTEVENTS = "Get Current Events"
    static let GETNEXTEVENTS = "Get Next Events"
    static let GETARTOFTHEMONTH = "Get Art of the Month"
    static let GETARTWORK = "Get Artwork"
    static let GETARTIST = "Get Artist"
    static let GETRELATESARTIST = "Get Relates Artist"
    static let GETARTISTSARTWORKS = "Get Artworks by Artist"
    static let GETEVENT = "Get Event"
    static let GETALLTAGS = "GET All Tags"
    static let GETGALLERYBYTAGS = "GET GALLERY BY TAGS"
    static let GETNEWS = "GET News"
    static let REGISTER = "Register"
    static let SAVEPROFILE = "Save Profile"
    static let GETACCOUNT = "Get Account"
    static let GETMUSEUM = "Get Museum"
    static let GETUSERVISITED = "Get User Visited"
    static let JOINEVENT = "Join Event"
    static let JOINEVENTFAIL = "Join Event Fail"
    static let SEARCH = "Search"
    
//    let defaultApi = "http://35.186.145.210"
    let defaultApi = "http://rcac.bangkokapp.co"
//    let defaultApi = "http://192.168.1.55:8080/" //test
    
//    var delegate:ServicesDelegete?
    var loading = NVActivityIndicatorView.init(frame: CGRect.init(x: 0, y: 0, width:60, height:60), type: NVActivityIndicatorType.circleStrokeSpin, color: UIColor.white, padding:15)
    var header = Alamofire.SessionManager.defaultHTTPHeaders
//    var h = Zhou.sharedInstance
    
    var configuration = URLSessionConfiguration.default
    var sessionManager:SessionManager!
    let userDefault = UserDefaults.standard
    
    private override init() {
        super.init()
        sessionManager = Alamofire.SessionManager(configuration: configuration)
        configuration.httpAdditionalHeaders = header
        sessionManager = Alamofire.SessionManager(configuration: configuration)
        sessionManager.adapter = MashapeHeadersadapter()
//        userDefault.set("", forKey: "h")
    }
    
    func authen(email:String, delegate:ServicesDelegete) {
        
        self.addLoading()
        let urlStr = defaultApi + "/api/authenticate"
        let parameters: Parameters = ["username": email,
                                      "rememberMe": true]
        
        sessionManager.request(urlStr, method:.post, parameters: parameters, encoding: JSONEncoding.default)
                .validate(contentType: ["application/json"])
                .responseData { response in
        
                print("Request: \(String(describing: response.request))")
                print("Response: \(String(describing: response.response))")
                print("Result: \(response.result)")
                print("Response: \(String(describing: response.data))")

                var json = JSON(response.data!)

                if String(describing: response.result) == "FAILURE" {
                    print("HEADER DIDN'T MAKE")
                    self.finishTaskAndCallback(from: urlStr, response: json, tag: Services.AUTHENFAIL, delegate: delegate)
                } else {
                    self.makeHeader(token: String(describing:json["id_token"]), from:urlStr, response:(response.data)!,  delegate: delegate)
                }
        }
    }
    
    func fbAuthen(fbt:String, delegate:ServicesDelegete) {
        
        self.addLoading()
        let urlStr = defaultApi + "/api/fb/authenticate?token=" + fbt
        
        sessionManager.request(urlStr, method:.post, encoding: JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in
                
                print("Request: \(String(describing: response.request))")
                print("Response: \(String(describing: response.response))")
                print("Result: \(response.result)")
                print("Response: \(String(describing: response.data))")
                
                var json = JSON(response.data!)
                
                if String(describing: response.result) == "FAILURE" {
                    print("HEADER DIDN'T MAKE")
                    self.finishTaskAndCallback(from: urlStr, response: json, tag: Services.AUTHENFAIL, delegate: delegate)
                } else {
                    self.makeHeader(token: String(describing:json["id_token"]), from:urlStr, response:(response.data)!,  delegate: delegate)
                }
        }
    }
    
    func makeHeader(token:String, from:String, response:Any, delegate:ServicesDelegete) {

//        h.k = token
        userDefault.set(token, forKey: "h")
//        self.header = [
//            "Authorization": "Bearer \(token)",
//            "Accept": "application/json"
//        ]
//        self.header["API-Version"] = "2.0"
//        self.header["Authorization"] = "Bearer \(token)"
        
        print("make headers : " + String(describing: header))
        self.finishTaskAndCallback(from: from, response: response, tag: Services.AUTHENSUCCESS, delegate: delegate)

    }
    
    func getAccount(delegate:ServicesDelegete) {
        
        self.addLoading()
        let urlStr = defaultApi + "/api/account"
        
        
        
        sessionManager.request(urlStr, method:.get, encoding:JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in
                debugPrint(response.request!)
                print("TOKEN: \(String(describing: response.request?.allHTTPHeaderFields))")
                print("User Account: \(JSON(response.data!))")
                
                let user = User.sharedInstance.makeUserObj(data: JSON(response.data!))
                self.finishTaskAndCallback(from: urlStr, response:user, tag: Services.GETACCOUNT, delegate: delegate)
        }
        
    }
    
    func getUserVisited(id:String, delegate:ServicesDelegete) {
        
        self.addLoading()
        let urlStr = defaultApi + "/api/m/visitors?userId=" + id
        
        sessionManager.request(urlStr, method:.get, encoding:JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in
                
                let eventList = NSMutableArray()
                for (_, val) in JSON(response.data!) {
                    print("\(val["name"])")
                    eventList.add(Event.init().makeEventObj(data: val))
                }
                
                self.finishTaskAndCallback(from: urlStr, response:eventList, tag: Services.GETUSERVISITED, delegate: delegate)
        }
    }
    
    func register(parameters:Parameters, delegate:ServicesDelegete) {
        
        self.addLoading()
        let urlStr = defaultApi + "/api/m/register"
        
        sessionManager.request(urlStr, method:.post, parameters: parameters, encoding: JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in
                print("Registered: \(JSON(response.data!))")
                var json = JSON(response.data!)
                if String(describing: response.result) == "FAILURE" {
                    print("HEADER DIDN'T MAKE")
                    self.finishTaskAndCallback(from: urlStr, response: json, tag: Services.AUTHENFAIL, delegate: delegate)
                } else {
                    self.makeHeader(token: String(describing:json["id_token"]), from:urlStr, response:(response.data)!,  delegate: delegate)
                }
                
//                self.finishTaskAndCallback(from: urlStr, response: JSON(response.data!), tag: Services.REGISTER, delegate: delegate)
        }
    }
    
    func saveProfile(parameters:Parameters, delegate:ServicesDelegete) {
        
        self.addLoading()
        let urlStr = defaultApi + "/api/account"
        
        sessionManager.request(urlStr, method:.post, parameters: parameters, encoding: JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in
                print("Save Profile: \(JSON(response.data!))")
                var json = JSON(response.data!)
//                if String(describing: response.result) == "FAILURE" {
//                    print("HEADER DIDN'T MAKE")
//                    self.finishTaskAndCallback(from: urlStr, response: json, tag: Services.AUTHENFAIL, delegate: delegate)
//                } else {
//                    self.makeHeader(token: String(describing:json["id_token"]), from:urlStr, response:(response.data)!,  delegate: delegate)
//                }
                
                self.finishTaskAndCallback(from: urlStr, response: JSON(response.data!), tag: Services.SAVEPROFILE, delegate: delegate)
        }
    }
    
    func signout(delegate:ServicesDelegete) {
        userDefault.set("", forKey: "h")
        let user = User.sharedInstance.makeUserObj(data: JSON())
        self.finishTaskAndCallback(from: "", response:user, tag: Services.SIGNOUT, delegate: delegate)
    }
    
    func activateUser(activateKey:String, delegate:ServicesDelegete) {
        
        self.addLoading()
        let urlStr = defaultApi + "/api/activate?key=" + activateKey
        
        sessionManager.request(urlStr, method:.get, encoding: JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in
                
                print("Activated: \(JSON(response.data!))")
                
//                self.finishTaskAndCallback(from: urlStr, response: JSON(response.data!), tag: Services., delegate: delegate)
        }
    }
    
    func getCurrentEvents(delegate:ServicesDelegete) {
        self.addLoading()
        let urlStr = defaultApi + "/api/m/events?period=NOW"
        sessionManager.request(urlStr, method:.get, encoding: JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in
                
                let eventList = NSMutableArray()
                for (_, val) in JSON(response.data!) {
                    print("\(val["name"])")
                    eventList.add(Event.init().makeEventObj(data: val))
                }
                self.finishTaskAndCallback(from: urlStr, response: eventList, tag: Services.GETCURRENTEVENTS, delegate: delegate)
        }
    }
    
    func getNextEvents(delegate:ServicesDelegete) {
        self.addLoading()
        let urlStr = defaultApi + "/api/m/events?period=NEXT"
        sessionManager.request(urlStr, method:.get, encoding: JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in
                
                let eventList = NSMutableArray()
                for (_, val) in JSON(response.data!) {
                    print("before make Event \(val["name"])")
                    eventList.add(Event.init().makeEventObj(data: val))
                }
                // TODO: send Array of Event instead of response.data
                self.finishTaskAndCallback(from: urlStr, response: eventList, tag: Services.GETNEXTEVENTS, delegate: delegate)
        }
    }
    
    func getArtoftheMonth(delegate:ServicesDelegete) {
        self.addLoading()
        let urlStr = defaultApi + "/api/m/artworks/artOfMonth"
        sessionManager.request(urlStr, method:.get, encoding: JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in
                
                let artwork = NSMutableArray()
                for (_, val) in JSON(response.data!) {
                    artwork.add(Artwork.init().makeEventObj(data: val))
                }
                // TODO: send Array of Event instead of response.data
                self.finishTaskAndCallback(from: urlStr, response: artwork, tag: Services.GETARTOFTHEMONTH, delegate: delegate)
        }
    }
    
    func getArtworkBy(id:String, delegate:ServicesDelegete) {
        self.addLoading()
        let urlStr = defaultApi + "/api/m/artworks/" + id
        sessionManager.request(urlStr, method:.get, encoding: JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in
            
                self.finishTaskAndCallback(from: urlStr, response: Artwork.init().makeEventObj(data: JSON(response.data!)), tag: Services.GETARTWORK, delegate: delegate)
        }
    }
    
    func getEventBy(id:String, delegate:ServicesDelegete) {
        self.addLoading()
        let urlStr = defaultApi + "/api/m/events/" + id
        sessionManager.request(urlStr, method:.get, encoding: JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in
                
                self.finishTaskAndCallback(from: urlStr, response: Event.init().makeEventObj(data: JSON(response.data!)), tag: Services.GETEVENT, delegate: delegate)
        }
    }
    
    func joinEvent(eventId:String, delegate:ServicesDelegete) {
        self.addLoading()
        let urlStr = defaultApi + "/api/m/events/" + eventId + "/audiences/" + (User.sharedInstance.id ?? "")
        
        sessionManager.request(urlStr, method:.post, encoding: JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in
                print("request: \(urlStr)")
                if JSON(response.data!)["status"] != JSON.null {
                    self.finishTaskAndCallback(from: urlStr, response: JSON(response.data!), tag: Services.JOINEVENTFAIL, delegate: delegate)
                } else {
                    self.finishTaskAndCallback(from: urlStr, response: JoinResponse.init().makeObj(data: JSON(response.data!)), tag: Services.JOINEVENT, delegate: delegate)
                }
        }
    }
    
    func getArtistBy(id:String, delegate:ServicesDelegete) {
        self.addLoading()
        let urlStr = defaultApi + "/api/m/artists/" + id
        sessionManager.request(urlStr, method:.get, encoding: JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in
                
                self.finishTaskAndCallback(from: urlStr, response: Artist.init().makeEventObj(data: JSON(response.data!)), tag: Services.GETARTIST, delegate: delegate)
        }
    }
    
    func getArtistRelates(id:String, delegate:ServicesDelegete) {
        self.addLoading()
        let urlStr = defaultApi + "/api/m/artists/" + id + "/relates"
        print(urlStr)
        sessionManager.request(urlStr, method:.get, encoding: JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in
                
                var res = JSON(response.data!)
                let ready = NSMutableArray()
                
                if res["artworks"].count >= 0 {
                    let searchObj = SearchObj.init()
                    searchObj.sectionName = SearchObj.SectionName.ARTWORKS
                    for (_, val) in res["artworks"] {
                        searchObj.sectionObjects.add(Artwork.init().makeEventObj(data: val))
                    }
                    ready.add(searchObj)
                }
                if res["artists"].count >= 0 {
                    let searchObj = SearchObj.init()
                    searchObj.sectionName = SearchObj.SectionName.ARTISTS
                    for (_, val) in res["artists"] {
                        searchObj.sectionObjects.add(Artist.init().makeEventObj(data: val))
                    }
                    ready.add(searchObj)
                }
                if res["events"].count >= 0 {
                    let searchObj = SearchObj.init()
                    searchObj.sectionName = SearchObj.SectionName.EVENTS
                    for (_, val) in res["events"] {
                        searchObj.sectionObjects.add(Event.init().makeEventObj(data: val))
                    }
                    ready.add(searchObj)
                }
                
                self.finishTaskAndCallback(from: urlStr, response: ready, tag: Services.GETRELATESARTIST, delegate: delegate)
        }
    }
    
    func getArtworksByArtist(id:String, delegate:ServicesDelegete) {
        self.addLoading()
        let urlStr = defaultApi + "/api/m/artists/" + id + "/artworks"
        sessionManager.request(urlStr, method:.get, encoding: JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in
                
                let artwork = NSMutableArray()
                for (_, val) in JSON(response.data!) {
                    artwork.add(Artwork.init().makeEventObj(data: val))
                }
                
                self.finishTaskAndCallback(from: urlStr, response: artwork, tag: Services.GETARTISTSARTWORKS, delegate: delegate)
        }
    }
    
    func getAllTags(delegate:ServicesDelegete) {
        self.addLoading()
        let urlStr = defaultApi + "/api/m/tags"
        sessionManager.request(urlStr, method:.get, encoding: JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in
                
                print("get all tag: \(String(describing: response.request))")
                let tags = NSMutableArray()
                for (_, val) in JSON(response.data!) {
                    tags.add(Tag.init().setTag(data: val))
                }
                self.finishTaskAndCallback(from: urlStr, response: tags, tag: Services.GETALLTAGS, delegate: delegate)
        }
    }
    
    func getGalleryByTags(ids:Array<String>, delegate:ServicesDelegete) {
        
        self.addLoading()
        var urlStr = defaultApi + "/api/m/artworks/tags"
        for id in ids {
            if ids.index(of: id) == 0 {
                urlStr += "?id=\(id)"
            } else {
                urlStr += "&id=\(id)"
            }
        }
        print("get Artwork by Tags: \(urlStr)")
        sessionManager.request(urlStr, method:.get, encoding: JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in

                let galleries = NSMutableArray()
                for (_, val) in JSON(response.data!) {
                    galleries.add(Gallery.init().makeEventObj(data: val))
                }
                // TODO: send Array of Event instead of response.data
                self.finishTaskAndCallback(from: urlStr, response: galleries, tag: Services.GETGALLERYBYTAGS, delegate: delegate)
        }
    }
    
    func getNews(delegate:ServicesDelegete) {
        self.addLoading()
        let urlStr = defaultApi + "/api/m/news"
        sessionManager.request(urlStr, method:.get, encoding: JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in
                
                let news = NSMutableArray()
                for (_, val) in JSON(response.data!) {
                    news.add(News.init().makeNewObj(data: val))
                }
                // TODO: send Array of Event instead of response.data
                self.finishTaskAndCallback(from: urlStr, response: news, tag: Services.GETNEWS, delegate: delegate)
        }
    }
    
    func search(q:String, delegate:ServicesDelegete) {
        self.addLoading()
        let urlStr = defaultApi + "/api/m/search/?q=" + q.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        print("\(q) \(urlStr)")
        sessionManager.request(urlStr, method:.get, encoding: JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in
                
                var res = JSON(response.data!)
                let ready = NSMutableArray()
                
                if res["artworks"].count >= 0 {
                    let searchObj = SearchObj.init()
                    searchObj.sectionName = SearchObj.SectionName.ARTWORKS
                    for (_, val) in res["artworks"] {
                        searchObj.sectionObjects.add(Artwork.init().makeEventObj(data: val))
                    }
                    ready.add(searchObj)
                }
                if res["artists"].count >= 0 {
                    let searchObj = SearchObj.init()
                    searchObj.sectionName = SearchObj.SectionName.ARTISTS
                    for (_, val) in res["artists"] {
                        searchObj.sectionObjects.add(Artist.init().makeEventObj(data: val))
                    }
                    ready.add(searchObj)
                }
                if res["events"].count >= 0 {
                    let searchObj = SearchObj.init()
                    searchObj.sectionName = SearchObj.SectionName.EVENTS
                    for (_, val) in res["events"] {
                        searchObj.sectionObjects.add(Event.init().makeEventObj(data: val))
                    }
                    ready.add(searchObj)
                }

                self.finishTaskAndCallback(from: urlStr, response: ready, tag: Services.SEARCH, delegate: delegate)
                
        }
    }
    
    func getMuseum(id:String, delegate:ServicesDelegete) {
        
        self.addLoading()
        let urlStr = defaultApi + "/api/m/museums/" + id
        sessionManager.request(urlStr, method:.get, encoding: JSONEncoding.default)
            .validate(contentType: ["application/json"])
            .responseData { response in
                
                print("get all tag: \(String(describing: response.request))")
                let museum = Museum.init().makeMuseumObj(data: JSON(response.data!))
//                for (_, val) in JSON(response.data!) {
//                    tags.add(Tag.init().setTag(data: val))
//                }
                self.finishTaskAndCallback(from: urlStr, response: museum, tag: Services.GETMUSEUM, delegate: delegate)
        }
        
    }

    //------------------------------------- UTILITIES -------------------------------------------//
    
    func addLoading() {
        loading.backgroundColor = UIColor(red:0.98, green:0.85, blue:0.39, alpha:1.00)
        loading.layer.cornerRadius = 5
        loading.center = CGPoint.init(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2)
        UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(loading)
        self.loading.startAnimating()
    }
    func removeLoading() {
        self.loading.removeFromSuperview()
        loading.stopAnimating()
    }
    
    func finishTaskAndCallback(from:String, response:Any, tag:String, delegate:ServicesDelegete?) {
        removeLoading()
        print("servics delegate \(String(describing: delegate))" )
        delegate?.callback(from: from, response: response, tag: tag)
    }
    
}
