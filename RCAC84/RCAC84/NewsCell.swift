//
//  NewsCell.swift
//  RCAC84
//
//  Created by Ch on 10/7/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {
    
    @IBOutlet weak var detail: UILabel!
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var bgview: UIView!
    @IBOutlet var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        bgview.layer.cornerRadius = 8
        bgview.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
