//
//  termOfServiceViewController.swift
//  RCAC84
//
//  Created by Ch on 20/9/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit

class termOfServiceViewController: UIViewController {

    let services = Services.sharedInstance
    @IBOutlet weak var webview: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUnique(navitems: self.navigationItem)
        
        services.getMuseum(id: "1", delegate: self)
        
    }
    
}

extension termOfServiceViewController:ServicesDelegete {
    func callback(from: String, response: Any, tag: String) {
        if tag == Services.GETMUSEUM {
            let museum = response as! Museum
            webview.loadRequest(URLRequest(url:URL.init(string: museum.termOfServiceUrl)!))
        }
    }
    
    
}
