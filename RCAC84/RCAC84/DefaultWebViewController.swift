//
//  DefaultWebViewController.swift
//  RCAC84
//
//  Created by Ch on 13/1/2562 BE.
//  Copyright © 2562 Bangkok App. All rights reserved.
//

import UIKit
import WebKit
import Foundation

class DefaultWebViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet var webview: UIWebView!
    var stringUrl : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let url = URL(string: stringUrl) else { return }
        self.webview.loadRequest(URLRequest(url: url))
    }
    
//    func openUrl(strUrl:String!) {
//        viewDidLoad()
//        guard let url = URL(string: strUrl) else { return }
//        self.webview.loadRequest(URLRequest(url: url))
//    }

}
