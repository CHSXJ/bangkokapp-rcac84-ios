//
//  EventCollectionViewCell.swift
//  RCAC84
//
//  Created by Ch on 18/4/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import TagListView

class EventCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var poster: UIImageView!
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    @IBOutlet weak var eventDetail: UILabel!
    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var eventView: UIView!
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var agendaButton: UIButton!
    let view = AgendaViewController()
    var eventId = ""
    
    
    var delegate: JoinEventDelegate?
    
    @IBOutlet weak var agendaStackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.eventView.layer.cornerRadius = 10
        self.eventView.layer.masksToBounds = true
        tagListView.alignment = .left
        tagListView.clipsToBounds = true
        
        agendaButton.layer.shadowColor = UIColor.black.cgColor
        agendaButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        agendaButton.layer.masksToBounds = false
        agendaButton.layer.shadowRadius = 3.0
        agendaButton.layer.shadowOpacity = 0.5
        agendaButton.layer.cornerRadius = agendaButton.frame.size.width / 2

    }
    
    func setAgendas(eventDate:Array<EventDate>) {
        print("setAgenda\(eventDate)")
        self.view.eventDates = eventDate
//        self.agendaStackView.arrangedSubviews.forEach({ $0.removeFromSuperview() })
        for date in eventDate {
//            let agendaView = AgendaViewController()
            print(date.instant)
            print(date.agendas)
//            agendaView.set(header: agenda.name, time: agenda.time, detail: agenda.detail)
//            self.agendaStackView.addArrangedSubview(agendaView.view)
        }
    }
    
    func setTage(tags:Array<Tag>) {
        
        tagListView.removeAllTags()
        for tag in tags {
            tagListView.addTag(tag.name)
        }
//        tagListView.layoutIfNeeded()
    }

    @IBAction func pressJoinEvent(_ sender: Any) {
        delegate?.join(eventId: eventId)
    }
    
    @IBAction func seeAgenda(_ sender: Any) {
//        self.superview?.addSubview(self.view.view)
        let window = UIApplication.shared.keyWindow!
        self.view.view.frame = window.frame
        window.addSubview(self.view.view)
        
    }
}

//extension EventCollectionViewCell:AgendaDelegete {
//    func displayAgenda(dateList: Array<EventDate>) {
//        delegate?.displayAgenda(dateList: dateList)
//    }
//
//}
