//
//  LocationViewController.swift
//  RCAC84
//
//  Created by Ch on 20/9/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit

class LocationViewController: UIViewController {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var openingHours: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var mapView: UIImageView!
    
    let services = Services.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUnique(navitems: self.navigationItem)
        services.getMuseum(id: "1", delegate: self)
    }

}

extension LocationViewController:ServicesDelegete {
    func callback(from: String, response: Any, tag: String) {
        if tag == Services.GETMUSEUM {
            let museum = response as! Museum
            name.text = museum.name
            openingHours.text = museum.openingHours
            address.text = museum.address
            phoneNumber.text = museum.phoneNumber
            email.text = museum.email
            mapView.kf.setImage(with: URL(string: museum.mapURL))
        }
    }
    
    
}
