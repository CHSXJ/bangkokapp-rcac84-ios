//
//  ArtistViewController.swift
//  RCAC84
//
//  Created by Ch on 24/6/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import AVFoundation
import PagingKit
import Kingfisher
import FlagKit

class ArtistViewController: UIViewController {

    var services = Services.sharedInstance
    var menuViewController: PagingMenuViewController?
    var contentViewController: PagingContentViewController?
//    var artist = Artist()
    var artistId = ""
    
    @IBOutlet weak var artistImage: UIImageView!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var artistAge: UILabel!
    @IBOutlet weak var flag: UIImageView!
    
    static var sizingCell = TitleLabelMenuViewCell(frame: CGRect(x: 0, y: 0, width: 1, height: 1))
    
    let dataSource: [(menu: String, content: UIViewController)] = [(menu: "PROFILE", content: ArtistProfileViewController()), (menu: "ARTWORKS", content: ArtistsArtworksViewController()), (menu: "RELATED", content: ArtistRelatedViewController())]
    
    lazy var firstLoad: (() -> Void)? = { [weak self, menuViewController, contentViewController] in
        menuViewController?.reloadData()
        contentViewController?.reloadData()
        self?.firstLoad = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUnique(navitems: self.navigationItem)
        
        services.getArtistBy(id: self.artistId, delegate: self)
        services.getArtworksByArtist(id: self.artistId, delegate: self)
        services.getArtistRelates(id: self.artistId, delegate: self)
        
        menuViewController?.register(type: TitleLabelMenuViewCell.self, forCellWithReuseIdentifier: "identifier")
        let focusview = UnderlineFocusView()
        focusview.underlineColor = UIColor.init(red: 30/255, green: 35/255, blue: 45/255, alpha: 1)
        menuViewController?.registerFocusView(view: focusview)
        menuViewController?.focusView.backgroundColor = .clear
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        firstLoad?()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PagingMenuViewController {
            menuViewController = vc
            menuViewController?.dataSource = self
            menuViewController?.delegate = self
        } else if let vc = segue.destination as? PagingContentViewController {
            contentViewController = vc
            contentViewController?.delegate = self
            contentViewController?.dataSource = self
        }
    }
    
}

extension ArtistViewController: PagingMenuViewControllerDataSource {
    
    func menuViewController(viewController: PagingMenuViewController, cellForItemAt index: Int) -> PagingMenuViewCell {
        let cell = viewController.dequeueReusableCell(withReuseIdentifier: "identifier", for: index)  as! TitleLabelMenuViewCell
        cell.titleLabel.text = dataSource[index].menu
        cell.titleLabel.font = UIFont.systemFont(ofSize: 15)
        cell.backgroundColor = .clear
        cell.focusColor = UIColor.init(red: 30/255, green: 35/255, blue: 45/255, alpha: 1)
        return cell
    }
    
    func menuViewController(viewController: PagingMenuViewController, widthForItemAt index: Int) -> CGFloat {
        MainViewController.sizingCell.titleLabel.text = dataSource[index].menu
        var referenceSize = UILayoutFittingCompressedSize
        referenceSize.height = viewController.view.bounds.height
        let size = MainViewController.sizingCell.systemLayoutSizeFitting(referenceSize)
        return size.width
    }
    
    var insets: UIEdgeInsets {
        if #available(iOS 11.0, *) {
            return view.safeAreaInsets
        } else {
            return .zero
        }
    }
    
    func numberOfItemsForMenuViewController(viewController: PagingMenuViewController) -> Int {
        return dataSource.count
    }
}

extension ArtistViewController: PagingContentViewControllerDataSource {
    func numberOfItemsForContentViewController(viewController: PagingContentViewController) -> Int {
        return dataSource.count
    }
    
    func contentViewController(viewController: PagingContentViewController, viewControllerAt Index: Int) -> UIViewController {
        return dataSource[Index].content
    }
    
}

extension ArtistViewController: PagingMenuViewControllerDelegate {
    func menuViewController(viewController: PagingMenuViewController, didSelect page: Int, previousPage: Int) {
        contentViewController?.scroll(to: page, animated: true)
    }
}

extension ArtistViewController: PagingContentViewControllerDelegate {
    func contentViewController(viewController: PagingContentViewController, didManualScrollOn index: Int, percent: CGFloat) {
        menuViewController?.scroll(index: index, percent: percent, animated: false)
    }
}

extension ArtistViewController:ServicesDelegete {
    
    func callback(from: String, response: Any, tag: String) {
        
        if tag == Services.GETARTIST {
            
            let artist = response as! Artist
            
            let url = URL.init(string: artist.profilePic)
            self.artistImage.kf.setImage(with: url)
            self.artistName.text = artist.nameen
            self.artistAge.text = ""
            //        let countryCode = Locale.current.regionCode!
            let bundle = FlagKit.assetBundle
//            self.flag.image = UIImage(named: "TH", in: bundle, compatibleWith: nil)
            if artist.nationality != "" {
                self.flag.image = UIImage(named: artist.nationality.uppercased(), in: bundle, compatibleWith: nil)
            } else {
                self.flag.image = UIImage(named: "", in: bundle, compatibleWith: nil)
            }

            
            let profileView = dataSource[0].content as! ArtistProfileViewController
            profileView.exp?.text = artist.experience
            profileView.edu?.text = artist.education
            profileView.awards?.text = artist.awards
            profileView.story?.text = artist.story
            profileView.email?.text = "Email : " + artist.email
            profileView.mobile?.text = "Tel. : " + artist.mobile
            
        } else if tag == Services.GETARTISTSARTWORKS {
            
            let artworks = response as! Array<Artwork>
            let artworksView = dataSource[1].content as! ArtistsArtworksViewController
            artworksView.items = artworks
            artworksView.galleryCollectionView.reloadData()
            
        } else if tag == Services.GETRELATESARTIST {
            
            let res = response as! Array<SearchObj>
            let relatesView = dataSource[2].content as! ArtistRelatedViewController
            
            for r in res {
                r.toString()
            }
            if res.count > 0 {
                relatesView.items = res
            }
//            relatesView.items = res
//            relatesView.relatesTable!.reloadData()
        }
        
    }
}
