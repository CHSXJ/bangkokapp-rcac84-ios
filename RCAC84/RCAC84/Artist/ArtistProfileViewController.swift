//
//  ArtistProfileViewController.swift
//  RCAC84
//
//  Created by Ch on 24/6/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit

class ArtistProfileViewController: UIViewController {
    
    @IBOutlet weak var awards: UILabel?
    @IBOutlet weak var exp: UILabel?
    @IBOutlet weak var edu: UILabel?
    @IBOutlet weak var story: UILabel?
    @IBOutlet weak var email: UILabel?
    @IBOutlet weak var mobile: UILabel?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
