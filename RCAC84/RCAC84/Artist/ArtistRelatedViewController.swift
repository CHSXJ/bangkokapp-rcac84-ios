//
//  ArtistRelatedViewController.swift
//  RCAC84
//
//  Created by Ch on 24/6/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit

class ArtistRelatedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var relatesTable: UITableView!
    
    var services = Services.sharedInstance
    var items = Array<SearchObj>()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        relatesTable.register(UINib.init(nibName: "SearchTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchCell")
        relatesTable.register(UINib.init(nibName: "PreviousEventTableViewCell", bundle: nil), forCellReuseIdentifier: "PVEvent")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        relatesTable.reloadData()
        for item in items {
            for i in item.sectionObjects {
                print("\(item) \(i)")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if items[section].sectionName == SearchObj.SectionName.EVENTS {
            return items[section].sectionObjects.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return items[section].sectionName.rawValue
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if items[indexPath.section].sectionName == SearchObj.SectionName.EVENTS {
            var events = items[indexPath.section].sectionObjects as! Array<Event>
            //        if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PVEvent", for: indexPath) as! PreviousEventTableViewCell
            cell.date.text = events[indexPath.row].dateString
            cell.name.text = events[indexPath.row].name
            cell.detail.text = events[indexPath.row].detail
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! SearchTableViewCell
            cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
            if items[indexPath.section].sectionName == SearchObj.SectionName.ARTWORKS {
                cell.galleryCollectionView.tag = 11
            } else if items[indexPath.section].sectionName == SearchObj.SectionName.ARTISTS {
                print("tag = 22 : \(items[indexPath.section].sectionName)")
                cell.galleryCollectionView.tag = 22
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if items[section].sectionObjects.count <= 0 {
            return 0
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if items[indexPath.section].sectionObjects.count <= 0 {
            return 0
        } else {
            if items[indexPath.section].sectionName == SearchObj.SectionName.EVENTS {
                return 74
            }
            let x = Int(tableView.frame.size.width/4) * Int((items[indexPath.section].sectionObjects.count/4) + 1)
            return CGFloat.init(x)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("did selected tableView cell")
        
        if indexPath.section == 2 {
            let vc = EventDetailViewController()
            let event = items[2].sectionObjects[indexPath.row] as! Event
            vc.eventId = event.id
            print("You selected event #\(event.id)!")
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

}

extension ArtistRelatedViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("did selected collectionview cell")
        
        if collectionView.tag == 11 {
            let vc = ArtworkViewController()
            vc.artworkId = (items[0].sectionObjects[indexPath.row] as! Artwork).id
            //            print("You selected artwork #\(artoftheMonth[indexPath.row].id)!")
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if collectionView.tag == 22 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ArtistViewController") as! ArtistViewController
            vc.artistId = (items[1].sectionObjects[indexPath.row] as! Artist).id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 11 {
            print("numberOfItemsInSection \(0):\(items[0].sectionName) \(items[0].sectionObjects.count)")
            return items[0].sectionObjects.count
        } else if collectionView.tag == 22 {
            print("numberOfItemsInSection \(1):\(items[1].sectionName) \(items[1].sectionObjects.count)")
            return items[1].sectionObjects.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gridCell", for: indexPath as IndexPath) as! ImageCollectionViewCell
        
        if collectionView.tag == 11 { //ARTWORK
            let url = URL(string: (items[0].sectionObjects[indexPath.row] as! Artwork).image)!
            cell.image.kf.setImage(with: url)
        } else if collectionView.tag == 22 { //ARTIST
            print("tag == 2 \(items[1].sectionObjects[indexPath.row])")
            let url = URL(string: (items[1].sectionObjects[indexPath.row] as! Artist).profilePic)!
            cell.image.kf.setImage(with: url)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width/4, height: collectionView.frame.size.width/4)
    }
    
}
