//
//  ArtistsArtworksViewController.swift
//  RCAC84
//
//  Created by Ch on 24/6/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit

class ArtistsArtworksViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {
    
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    @IBOutlet weak var AOMH: NSLayoutConstraint!
    var items = Array<Artwork>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        galleryCollectionView.register(UINib.init(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "gridCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        galleryCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let h = 150*((items.count/4) + 1)
        self.AOMH.constant = CGFloat(h)
        print("Artwork Count \(items.count)")
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gridCell", for: indexPath as IndexPath) as! ImageCollectionViewCell
        let url = URL(string: items[indexPath.row].image)!
        cell.image.kf.setImage(with: url)
        
        return cell
    
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/3, height: collectionView.frame.size.width/3)
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = ArtworkViewController()
        vc.artworkId = items[indexPath.row].id
        print("You selected artwork #\(items[indexPath.row].id)!")
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
