//
//  AgendaViewController.swift
//  RCAC84
//
//  Created by Ch on 19/9/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit

class AgendaViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bg: UIView!
    var eventDates = Array<EventDate>()
    
    @IBAction func close(_ sender: Any) {
        view.removeFromSuperview()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib.init(nibName: "PreviousEventTableViewCell", bundle: nil), forCellReuseIdentifier: "PVEvent")
        tableView.layer.cornerRadius = 10
        tableView.clipsToBounds = true
        
        bg.layer.cornerRadius = 10
        bg.clipsToBounds = true
    }

    // MARK: - Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return eventDates.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if eventDates[section].agendas.count == 0 {
            return 1
        } else {
            return eventDates[section].agendas.count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return eventDates[section].dateStr
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PVEvent", for: indexPath) as! PreviousEventTableViewCell
        
        if eventDates[indexPath.section].agendas.count == 0 {
            cell.date.text = ""
            cell.name.text = " No schedule for today."
            cell.detail.text = ""
        } else {
            cell.date.text = eventDates[indexPath.section].agendas[indexPath.row].time
            cell.name.text = eventDates[indexPath.section].agendas[indexPath.row].name
            cell.detail.text = eventDates[indexPath.section].agendas[indexPath.row].detail
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("did selected tableView cell")
//        let vc = EventDetailViewController()
//        let event = events[indexPath.row]
//        vc.eventId = event.id
//        print("You selected event #\(event.id)!")
//        self.navigationController?.pushViewController(vc, animated: true)
        
    }

}
