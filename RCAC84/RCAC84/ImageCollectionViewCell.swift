//
//  ImageCollectionViewCell.swift
//  RCAC84
//
//  Created by Ch on 16/4/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
