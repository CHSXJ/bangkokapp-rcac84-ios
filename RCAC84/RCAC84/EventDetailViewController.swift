//
//  EventDetailViewController.swift
//  RCAC84
//
//  Created by Ch on 4/6/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import Kingfisher
import ImageSlideshow

class EventDetailViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var artistH: NSLayoutConstraint!
    @IBOutlet weak var artworkH: NSLayoutConstraint!
    @IBOutlet weak var carouselView: ImageSlideshow!
    @IBOutlet weak var playButton: UIButton!
    
    var eventId = ""
    var event = Event()
    let services = Services.sharedInstance
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var detail: UILabel!
    
    @IBOutlet weak var artistsCollectionView: UICollectionView!
    @IBOutlet weak var artworksCollectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        services.getEventBy(id: eventId, delegate: self)
        
        artistsCollectionView.register(UINib.init(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "gridCell")
        artworksCollectionView.register(UINib.init(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "gridCell")
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
        playButton.isHidden = true
        carouselView.addGestureRecognizer(gestureRecognizer)
        carouselView.slideshowInterval = 3
        carouselView.contentScaleMode = .scaleAspectFill
        carouselView.currentPageChanged = { page in
            print("current page:", page)
            if self.event.mediaList[page].type != Media.IMAGE {
                self.playButton.isHidden = false
            } else {
                self.playButton.isHidden = true
            }
        }
        carouselView.bringSubview(toFront: playButton)
    }
    
    @objc func didTap() {
        carouselView.presentFullScreenController(from: self)
    }
    @IBAction func playMedia(_ sender: Any) {
        let media = self.event.mediaList[carouselView.currentPage]
        if media.type != Media.IMAGE {
            let wv = DefaultWebViewController.init()
            wv.stringUrl = media.url
            self.navigationController?.pushViewController(wv, animated: true)
//            wv.openUrl(strUrl: media.url)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupUnique(navitems: self.navigationItem)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        carouselView.slideshowInterval = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == artistsCollectionView) {
            if event.sampleArtists.count > 6 {
                return 6
            } else {
                return event.sampleArtists.count
            }
        } else {
            if event.sampleArtists.count > 6 {
                return 6
            } else {
                return event.sampleArtworks.count
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gridCell", for: indexPath as IndexPath) as! ImageCollectionViewCell
        
        if(collectionView == artistsCollectionView) {
            let url = URL(string: event.sampleArtists[indexPath.row].imageURL)!
            cell.image.kf.setImage(with: url)
            
        } else {
            let url = URL(string: event.sampleArtworks[indexPath.row].imageURL)!
            cell.image.kf.setImage(with: url)
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width/3, height: collectionView.frame.size.width/3)
    }

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if(collectionView == artistsCollectionView) {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ArtistViewController") as! ArtistViewController
            vc.artistId = event.sampleArtists[indexPath.row].id
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else {
            
            let vc = ArtworkViewController()
            vc.artworkId = event.sampleArtworks[indexPath.row].id
            print("You selected artwork #\(event.sampleArtworks[indexPath.row].id)!")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }

}

extension EventDetailViewController:ServicesDelegete {

    func callback(from: String, response: Any, tag: String) {

        if tag == Services.GETEVENT {

            self.event = response as! Event
            var imgUrlList = Array<KingfisherSource>()
            for media in event.mediaList {
                if media.type == Media.IMAGE {
                    imgUrlList.append(KingfisherSource(urlString: media.url)!)
                } else {
                    imgUrlList.append(KingfisherSource(urlString: media.thumbUrl)!)
                }
                media.toString()
            }
            carouselView.setImageInputs(imgUrlList)

            self.name.text = event.name
            self.date.text = event.eventDate
            self.detail.text = event.detail
            
            self.artistsCollectionView.reloadData()
            self.artworksCollectionView.reloadData()
            
            let h = (self.view.frame.size.width/3)*2
            self.artworkH.constant = h
            self.artistH.constant = h
        }

    }

}

extension String {
    var youtubeID: String? {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        
        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        let range = NSRange(location: 0, length: count)
        
        guard let result = regex?.firstMatch(in: self, options: [], range: range) else {
            return nil
        }
        
        return (self as NSString).substring(with: result.range)
    }
}

//extension ImageSlideshow {
//    func currentPageChanged() {
//        print("current page changed")
//    }
//}
