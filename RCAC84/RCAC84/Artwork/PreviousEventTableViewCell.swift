//
//  PreviousEventTableViewCell.swift
//  RCAC84
//
//  Created by Ch on 16/7/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit

class PreviousEventTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var detail: UILabel!
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var bgview: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        bgview.layer.cornerRadius = 5
        bgview.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
