//
//  ArtworkViewController.swift
//  RCAC84
//
//  Created by Ch on 19/4/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import TagListView

class ArtworkViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var artworkId = String()
    var artwork = Artwork()
    let services = Services.sharedInstance
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var artist: UILabel!
    @IBOutlet weak var technique: UILabel!
    @IBOutlet weak var size: UILabel!
    @IBOutlet weak var detail: UILabel!
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var previousEventsTable: UITableView!
    
    @IBOutlet weak var artistImage: UIImageView!
    @IBOutlet weak var artistStory: UILabel!
    @IBOutlet weak var artistView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        services.getArtworkBy(id: artworkId, delegate: self)
        self.artistView.layer.cornerRadius = 8.0
        self.artistView.clipsToBounds = true
        previousEventsTable.register(UINib.init(nibName: "PreviousEventTableViewCell", bundle: nil), forCellReuseIdentifier: "PVEvent")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupUnique(navitems: self.navigationItem)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setTage(tags:Array<Tag>) {
        
        tagListView.removeAllTags()
        for tag in tags {
            tagListView.addTag(tag.name)
        }
        //        tagListView.layoutIfNeeded()
    }
    
    @IBAction func readMoreArtist() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ArtistViewController") as! ArtistViewController
        vc.artistId = artwork.artist.id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return artwork.events.count
//        return 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PVEvent", for: indexPath) as! PreviousEventTableViewCell
        cell.date.text = artwork.events[indexPath.row].dateString
        cell.name.text = artwork.events[indexPath.row].name
        cell.detail.text = artwork.events[indexPath.row].about
        return cell

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("did selected tableView cell")
        let vc = EventDetailViewController()
        let event = artwork.events[indexPath.row] 
        vc.eventId = event.id
        print("You selected event #\(event.id)!")
        self.navigationController?.pushViewController(vc, animated: true)

    }

}

extension ArtworkViewController:ServicesDelegete {
    
    func callback(from: String, response: Any, tag: String) {
        
        if tag == Services.GETARTWORK {
            
            artwork = response as! Artwork
            
            print("Artwork: \(artwork.events.count)")
            
            
            var url = URL.init(string: artwork.image)
            self.image.kf.setImage(with:url)
            self.name.text = artwork.name
            self.artist.text = artwork.artist.nameen
            self.technique.text = "เทคนิค :\n" + artwork.technique
            self.size.text = "ขนาด :" + artwork.size
            self.detail.text = "แนวความคิด :\n" + artwork.concept
            setTage(tags: artwork.tags)
            self.previousEventsTable.reloadData()
            
            url = URL.init(string: artwork.artist.profilePic)
            self.artistImage.kf.setImage(with: url)
            self.artistStory.text = artwork.artist.story
            
        }
    
    }
    
}
