//
//  ViewController.swift
//  RCAC84
//
//  Created by Ch on 13/4/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import SideMenu
import QRCodeReader
import AVFoundation
import PagingKit

//protocol PagerViewDelegate {
//    func didScrollTo(page:Int)
//}

class MainViewController: UIViewController, SideMenuDelegate {
    
    func openPage(_ page: Int) {
        didScrollTo(page: page)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    var menuViewController: PagingMenuViewController?
    var contentViewController: PagingContentViewController?
    
    static var sizingCell = TitleLabelMenuViewCell(frame: CGRect(x: 0, y: 0, width: 1, height: 1))
    
//    let dataSource: [(menu: String, content: UIViewController)] = ["WHAT'S NEW", "EVENTS", "GALLERY", "SEARCH"].map {
//        let title = $0
//        let vc = WhatsNewViewController()
//        return (menu: title, content: vc)
//    }
    
    let dataSource: [(menu: String, content: UIViewController)] = [(menu: "WHAT'S NEW", content: WhatsNewViewController()), (menu: "EVENTS", content: EventsViewController()), (menu: "GALLERY", content: GalleryViewController()), (menu: "SEARCH", content: SearchViewController())]
        
//    lazy var readerVC: QRCodeReaderViewController = {
//        let builder = QRCodeReaderViewControllerBuilder {
//            $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
//        }
//
//        return QRCodeReaderViewController(builder: builder)
//    }()
    
    lazy var firstLoad: (() -> Void)? = { [self, menuViewController, contentViewController] in
        menuViewController?.reloadData()
        contentViewController?.reloadData()
        self.firstLoad = nil
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
//        UINavigationBar *navigationBar = self.navigationController.navigationBar;
//        let navigationBar = self.navigationController?.navigationBar
//        navigationBar?.setupBarItem()
        self.setupBarItem()
        hideKeyboardWhenTappedAround()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "rcac-logo"), style:.plain, target: self, action: nil)
//        self.navigationController?.navigationItem.setupBarItem()
        
//        SideMenuManager.default.menuRightNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? UISideMenuNavigationController
//        SideMenuManager.default.menuFadeStatusBar = false
//        SideMenuManager.default.menuPresentMode = .menuSlideIn
//
//        // Enable gestures. The left and/or right menus must be set up above for these to work.
//        // Note that these continue to work on the Navigation Controller independent of the view controller it displays!
//        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
//        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view, forMenu:.right)
        
        menuViewController?.register(type: TitleLabelMenuViewCell.self, forCellWithReuseIdentifier: "identifier")
        let focusview = UnderlineFocusView()
        focusview.underlineColor = UIColor.init(red: 30/255, green: 35/255, blue: 45/255, alpha: 1)
        menuViewController?.registerFocusView(view: focusview)
        menuViewController?.focusView.backgroundColor = .clear
        
//        didScrollTo(page: 3)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        firstLoad?()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PagingMenuViewController {
            menuViewController = vc
            menuViewController?.dataSource = self
            menuViewController?.delegate = self
        } else if let vc = segue.destination as? PagingContentViewController {
            contentViewController = vc
            contentViewController?.delegate = self
            contentViewController?.dataSource = self
        } else if let nav = segue.destination as? UISideMenuNavigationController, let sideMenu = nav.topViewController as? SideMenuTableViewController {
                sideMenu.delegate = self
        }
    }
    
    func didScrollTo(page:Int) {
        print("didScrollTo \(page)")
        menuViewController?.scroll(index: page)
        contentViewController?.scroll(to: page, animated: true)
    }
    
}


//MARK: Main Extension

extension MainViewController: PagingMenuViewControllerDataSource {
    
    func menuViewController(viewController: PagingMenuViewController, cellForItemAt index: Int) -> PagingMenuViewCell {
        let cell = viewController.dequeueReusableCell(withReuseIdentifier: "identifier", for: index)  as! TitleLabelMenuViewCell
        cell.titleLabel.text = dataSource[index].menu
        cell.titleLabel.font = UIFont.systemFont(ofSize: 15)
        cell.backgroundColor = .clear
        cell.focusColor = UIColor.init(red: 30/255, green: 35/255, blue: 45/255, alpha: 1)
        return cell
    }
    
    func menuViewController(viewController: PagingMenuViewController, widthForItemAt index: Int) -> CGFloat {
        MainViewController.sizingCell.titleLabel.text = dataSource[index].menu
        var referenceSize = UILayoutFittingCompressedSize
        referenceSize.height = viewController.view.bounds.height
        let size = MainViewController.sizingCell.systemLayoutSizeFitting(referenceSize)
        return size.width
    }
    var insets: UIEdgeInsets {
        if #available(iOS 11.0, *) {
            return view.safeAreaInsets
        } else {
            return .zero
        }
    }
    
    func numberOfItemsForMenuViewController(viewController: PagingMenuViewController) -> Int {
        return dataSource.count
    }
}

extension MainViewController: PagingContentViewControllerDataSource {
    func numberOfItemsForContentViewController(viewController: PagingContentViewController) -> Int {
        return dataSource.count
    }
    
    func contentViewController(viewController: PagingContentViewController, viewControllerAt Index: Int) -> UIViewController {
        return dataSource[Index].content
    }
    
}

extension MainViewController: PagingMenuViewControllerDelegate {
    func menuViewController(viewController: PagingMenuViewController, didSelect page: Int, previousPage: Int) {
        contentViewController?.scroll(to: page, animated: true)
    }
}

extension MainViewController: PagingContentViewControllerDelegate {
    func contentViewController(viewController: PagingContentViewController, didManualScrollOn index: Int, percent: CGFloat) {
        menuViewController?.scroll(index: index, percent: percent, animated: false)
    }
}

extension UIViewController:QRCodeReaderViewControllerDelegate {
    
//    public func gotoPage(page:Int) {
//        self.navigationController?.dismiss(animated: true, completion: {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let mainView = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController
//            mainView?.didScrollTo(page: page)
//        })
//    }
    
    public func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        let code  = result.value.split(separator: ".")
        print("\(code[0]) \(code[1])")
        
        //        EVENT.5b439d99d06f5958e964933d
        dismiss(animated: true, completion: nil)
        
        if code[0] == "EVENT" {
            print("scaning found an Event")
            let mainView = self.navigationController?.viewControllers[0] as! MainViewController
            let view = mainView.dataSource[0].content as! WhatsNewViewController
            view.join(eventId: String(code[1]))
        } else if code[0] == "ARTWORK" {
            let vc = ArtworkViewController()
            vc.artworkId = String(code[1])
            self.navigationController?.pushViewController(vc, animated: true)
        } else if code[0] == "ARTIST" {
            let vc = ArtistViewController()
            vc.artistId = String(code[1])
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    public func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        print("dismissKeyboard")
        view.endEditing(true)
    }
//}
//
//extension UINavigationBar {
    
    func setupBarItem() {
        let qrButton = UIBarButtonItem.init(image: UIImage(named: "qr-code"), style: .plain, target: self, action: #selector(UIViewController.openQRCam))
        let sidemenuButton = UIBarButtonItem.init(image: UIImage(named: "menu"), style: .plain, target: self, action: #selector(UIViewController.showSidebar))
        
        // for storyboard viewcontroller
        navigationController?.navigationBar.topItem?.rightBarButtonItems = [sidemenuButton, qrButton]
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "rcac-logo"), style:.plain, target: self, action: nil)
        navigationController?.navigationBar.topItem?.backBarButtonItem?.title = ""
        navigationController?.navigationBar.topItem?.leftBarButtonItem = nil
    }
    
    func setupUnique(navitems:UINavigationItem) {
//        setupBarItem()
        let qrButton = UIBarButtonItem.init(image: UIImage(named: "qr-code"), style: .plain, target: self, action: #selector(UIViewController.openQRCam))
        let sidemenuButton = UIBarButtonItem.init(image: UIImage(named: "menu"), style: .plain, target: self, action: #selector(UIViewController.showSidebar))
        navitems.rightBarButtonItems = [sidemenuButton, qrButton]
        navitems.backBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "rcac-logo"), style:.plain, target: self, action: nil)
        navitems.backBarButtonItem?.title = ""
//        navitems.leftBarButtonItem = navitems.backBarButtonItem
    }
    
    @objc func openQRCam() {
        
        let readerVC: QRCodeReaderViewController = {
            let builder = QRCodeReaderViewControllerBuilder {
                $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            }

            return QRCodeReaderViewController(builder: builder)
        }()

        // Retrieve the QRCode content
        // By using the delegate pattern
        readerVC.delegate = self

        // Or by using the closure pattern
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
        }

        // Presents the readerVC as modal form sheet
        readerVC.modalPresentationStyle = .formSheet
        present(readerVC, animated: true, completion: nil)
    
    }
    
    
    @objc func showSidebar() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        SideMenuManager.default.menuRightNavigationController = storyboard.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? UISideMenuNavigationController
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        let sideMenu = SideMenuManager.default.menuRightNavigationController?.topViewController as? SideMenuTableViewController
        sideMenu?.delegate = self.navigationController?.viewControllers[0] as! MainViewController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the view controller it displays!
//        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
//        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view, forMenu:.right)
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
//        performSegue(withIdentifier: "sideMenu", sender: nil)
        
    }
}


