//
//  EditProfileViewController.swift
//  RCAC84
//
//  Created by Ch on 2/9/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import CountryPicker
import Alamofire

class EditProfileViewController: UIViewController, CountryPickerDelegate {
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        self.nationality.setTitle(name, for: .normal)
        self.selectedCode = countryCode
    }
    

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var titleName: UIButton!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var birthday: UIButton!
    @IBOutlet weak var nationality: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    
    let services = Services.sharedInstance
    var selectedCode = ""
    
    //get current country
    let picker = CountryPicker()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.setupUnique(navitems: self.navigationItem)
        self.hideKeyboardWhenTappedAround()
        self.saveBtn.layer.cornerRadius = 10
        self.saveBtn.clipsToBounds = true
        
        setProfile()
        
    }
    
    func setProfile() {
        
        let user = User.sharedInstance
        emailField.text = user.email
        titleName.setTitle(user.title, for: .normal)
        firstNameField.text = user.firstName
        lastNameField.text = user.lastName
        birthday.setTitle(user.birthDate, for: .normal)
        
        let theme = CountryViewTheme(countryCodeTextColor: .black, countryNameTextColor: .black, rowBackgroundColor: .white, showFlagsBorder: false)        //optional for UIPickerView theme changes
        picker.theme = theme //optional for UIPickerView theme changes
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = false
        
        if user.nationality == ""  {
            let locale = Locale.current
            let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
            selectedCode = code!
            picker.setCountry(code!)
        } else {
            selectedCode = user.nationality!.uppercased()
            picker.setCountry(user.nationality!.uppercased())
        }
        
    }
    
    @IBAction func pressSave(_ sender: Any) {
        
        let parameters:Parameters = ["email":emailField.text!,
                                     "login":emailField.text!,
                                     "firstName":firstNameField.text!,
                                     "lastName":lastNameField.text!,
                                     "birthDate":birthday.title(for: .normal)!,
                                     "nationality":selectedCode,
                                     "title":titleName.title(for: .normal)!,
                                     "subscribe": true
        ]
        
        print("\(parameters)")
        services.saveProfile(parameters: parameters, delegate: self)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func pressTitle(_ sender:Any) {
        
        let alert = UIAlertController(title: "Choose your name title", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Mr.", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            self.titleName.setTitle("Mr.", for: .normal)
        }))
        
        alert.addAction(UIAlertAction(title: "Ms.", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.titleName.setTitle("Ms.", for: .normal)
        }))
        
        alert.addAction(UIAlertAction(title: "Mrs.", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.titleName.setTitle("Mrs.", for: .normal)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Close button")
        }))
        
        self.present(alert, animated: true, completion:nil)
        
    }

    @IBAction func selectDate(_ sender: UIButton) {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        
        let alert = UIAlertController(title: "\n\n\n\n\n\n\n\n\n\n\n", message: nil, preferredStyle: .actionSheet)
        
        let margin:CGFloat = 10.0
        let rect = CGRect(x: margin, y: margin, width: alert.view.bounds.size.width - margin * 4.0, height: 220)
        datePicker.frame = rect
        
        alert.view.addSubview(datePicker)
        
        let ok = UIAlertAction(title: "Select", style: .default) { (action) in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let dateString = dateFormatter.string(from: datePicker.date)
            print(dateString)
            self.birthday.setTitle(dateString, for: .normal)
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(ok)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func selectCountry(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "\n\n\n\n\n\n\n\n\n\n\n", message: nil, preferredStyle: .actionSheet)
        
        let margin:CGFloat = 10.0
        let rect = CGRect(x: margin, y: margin, width: alert.view.bounds.size.width - margin * 4.0, height: 220)
        picker.frame = rect
        
        alert.view.addSubview(picker)
        
        alert.addAction(UIAlertAction(title: "Select", style: .default, handler:{ (UIAlertAction)in
            print("User click Close button")
        }))
        
        present(alert, animated: true, completion: nil)
    }

}

extension EditProfileViewController:ServicesDelegete {
    func callback(from: String, response: Any, tag: String) {
        if tag == Services.SAVEPROFILE {
            print("save here \(response)")
            services.getAccount(delegate: self)
        } else if tag == Services.GETACCOUNT {
        
            let alert = UIAlertController(title: "Your accout is updated!", message: nil, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Done", style: .default, handler:{ (UIAlertAction)in
//                self.navigationController?.popToRootViewController(animated: true)
            }))
            present(alert, animated: true, completion: nil)
            setProfile()
        }
    }
    
    
}
