//
//  ProfileViewController.swift
//  RCAC84
//
//  Created by Ch on 2/9/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import FlagKit

class ProfileViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let services = Services.sharedInstance
    let user = User.sharedInstance
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var editProfile: UIButton!
    @IBOutlet weak var signout: UIButton!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var flag: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    
    var events = Array<Event>()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let url = URL.init(string: user.imageUrl!)
        self.avatar.kf.setImage(with: url)
        self.name.text = user.firstName! + " " + user.lastName!
        self.age.text = user.birthDate
        
        let bundle = FlagKit.assetBundle
        if user.nationality != nil {
            self.flag.image = UIImage(named: user.nationality!.uppercased(), in: bundle, compatibleWith: nil)
        } else {
            self.flag.image = UIImage(named: "TH", in: bundle, compatibleWith: nil)
        }
        
        services.getUserVisited(id:user.id!, delegate: self)
        
        editProfile.layer.borderWidth = 0.8
        editProfile.layer.cornerRadius = 10
        editProfile.layer.masksToBounds = true
        editProfile.layer.borderColor = UIColor.init(red: 151.0/255.0, green: 151.0/255.0, blue: 151.0/255.0, alpha: 1).cgColor
        signout.layer.borderWidth = 0.8
        signout.layer.cornerRadius = 10
        signout.layer.masksToBounds = true
        signout.layer.borderColor = UIColor.init(red: 151.0/255.0, green: 151.0/255.0, blue: 151.0/255.0, alpha: 1).cgColor
        
        tableView.register(UINib.init(nibName: "PreviousEventTableViewCell", bundle: nil), forCellReuseIdentifier: "PVEvent")
//        tableView.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signout(_ sender: Any) {
        services.signout(delegate: self)
    }
    
    // MARK: - Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
        //        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PVEvent", for: indexPath) as! PreviousEventTableViewCell
        cell.date.text = events[indexPath.row].dateString
        cell.name.text = events[indexPath.row].name
        cell.detail.text = events[indexPath.row].detail
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("did selected tableView cell")
        let vc = EventDetailViewController()
        let event = events[indexPath.row]
        vc.eventId = event.id
        print("You selected event #\(event.id)!")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

extension ProfileViewController: ServicesDelegete {
    func callback(from: String, response: Any, tag: String) {
        if tag == Services.SIGNOUT {
            print("Sign out: \(response)")
            let alert = UIAlertController(title: "You've already sign out!", message: nil, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Close", style: .default, handler:{ (UIAlertAction) in
                self.navigationController?.popToRootViewController(animated: true)
            }))
            present(alert, animated: true, completion: nil)
        
        } else if tag == Services.GETUSERVISITED {
            events = response as! Array<Event>
            tableView.reloadData()
        }
        
    }
}
