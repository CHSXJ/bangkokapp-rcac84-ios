//
//  EmailSignupViewController.swift
//  RCAC84
//
//  Created by Ch on 13/7/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import Alamofire
import CountryPicker

class EmailSignupViewController: UIViewController, UIActionSheetDelegate, CountryPickerDelegate {
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        self.nationality.setTitle(name, for: .normal)
        selectedCode = countryCode
    }
    
    
    @IBOutlet weak var fieldEmail: UITextField!
    @IBOutlet weak var fieldFirstname: UITextField!
    @IBOutlet weak var fieldLastname: UITextField!
    @IBOutlet weak var signup: UIButton!
    
    @IBOutlet weak var titleName: UIButton!
    @IBOutlet weak var bday: UIButton!
    @IBOutlet weak var nationality: UIButton!
    
    let services = Services.sharedInstance
    var selectedCode = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        signup.layer.cornerRadius = 5.0
        signup.layer.masksToBounds = true
        signup.layer.cornerRadius = 5.0
        signup.layer.masksToBounds = true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func register(_ sender:Any) {
        let parameters:Parameters = ["email":fieldEmail.text!,
                                     "login":fieldEmail.text!,
                                     "firstName":fieldFirstname.text!,
                                     "lastName":fieldLastname.text!,
                                     "birthDate":bday.title(for: .normal)!,
                                     "nationality":selectedCode.uppercased(),
                                     "title":titleName.title(for: .normal)!,
                                     "subscribe": true
                                     ]
        services.register(parameters: parameters, delegate: self)
    }
    
    @IBAction func pressTitle(_ sender:Any) {
        
        let alert = UIAlertController(title: "Choose your name title", message: "", preferredStyle: .actionSheet)
        alert.popoverPresentationController?.sourceView = titleName.titleLabel
        
        alert.addAction(UIAlertAction(title: "Mr.", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            self.titleName.setTitle("Mr.", for: .normal)
        }))
        
        alert.addAction(UIAlertAction(title: "Ms.", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.titleName.setTitle("Ms.", for: .normal)
        }))
        
        alert.addAction(UIAlertAction(title: "Mrs.", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.titleName.setTitle("Mrs.", for: .normal)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Close button")
        }))
        
        self.present(alert, animated: true, completion:nil)
        
    }
    
    @IBAction func selectDate(_ sender: UIButton) {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        
        let alert = UIAlertController(title: "\n\n\n\n\n\n\n\n\n\n\n", message: nil, preferredStyle: .actionSheet)
        alert.popoverPresentationController?.sourceView = bday.titleLabel
        
        let margin:CGFloat = 10.0
        let rect = CGRect(x: margin, y: margin, width: alert.view.bounds.size.width - margin * 4.0, height: 220)
        datePicker.frame = rect
        
        alert.view.addSubview(datePicker)
        
        let ok = UIAlertAction(title: "Select", style: .default) { (action) in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let dateString = dateFormatter.string(from: datePicker.date)
            print(dateString)
            self.bday .setTitle(dateString, for: .normal)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(ok)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func selectCountry(_ sender: UIButton) {
        //get current country
        let picker = CountryPicker()
        
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        //init Picker
//        picker.displayOnlyCountriesWithCodes = ["DK", "SE", "NO", "DE"] //display only
//        picker.exeptCountriesWithCodes = ["RU"] //exept country
        let theme = CountryViewTheme(countryCodeTextColor: .black, countryNameTextColor: .black, rowBackgroundColor: .white, showFlagsBorder: false)        //optional for UIPickerView theme changes
        picker.theme = theme //optional for UIPickerView theme changes
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = true
        picker.setCountry(code!)
        
        let alert = UIAlertController(title: "\n\n\n\n\n\n\n\n\n\n\n", message: nil, preferredStyle: .actionSheet)
        alert.popoverPresentationController?.sourceView = nationality.titleLabel
        
        let margin:CGFloat = 10.0
        let rect = CGRect(x: margin, y: margin, width: alert.view.bounds.size.width - margin * 4.0, height: 220)
        picker.frame = rect
        
        alert.view.addSubview(picker)
        
        alert.addAction(UIAlertAction(title: "Select", style: .default, handler:{ (UIAlertAction)in
            print("User click Close button")
        }))
        
        present(alert, animated: true, completion: nil)
    }

}

extension EmailSignupViewController:ServicesDelegete {
    func callback(from: String, response: Any, tag: String) {
    
        if tag == Services.AUTHENSUCCESS {
            print("Register Callback here! \(response)")
            let alert = UIAlertController(title: "Register Complete!", message: nil, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Done", style: .default, handler:{ (UIAlertAction)in
                self.navigationController?.popToRootViewController(animated: true)
            }))
            present(alert, animated: true, completion: nil)

        } else if tag == Services.GETACCOUNT {
            
        }
    }
    
    
}
