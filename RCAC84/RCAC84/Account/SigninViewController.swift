//
//  SigninViewController.swift
//  RCAC84
//
//  Created by Ch on 20/5/2561 BE.
//  Copyright © 2561 Bangkok App. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FacebookShare

class SigninViewController: UIViewController {

    @IBOutlet weak var fieldEmail: UITextField!
    @IBOutlet weak var signinEmail: UIButton!
    @IBOutlet weak var signupEmail: UIButton!
    @IBOutlet weak var signupFacebook: UIButton!
    
    let services = Services.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        signinEmail.layer.cornerRadius = 5.0
        signinEmail.layer.masksToBounds = true
        signupEmail.layer.cornerRadius = 5.0
        signupEmail.layer.masksToBounds = true
        signupFacebook.layer.cornerRadius = 5.0
        signupFacebook.layer.masksToBounds = true
        
        if let accessToken = AccessToken.current {
            // User is logged in, use 'accessToken' here.
            print("ACCESS TOKEN: \(accessToken)")
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
    }
    
    @IBAction func signin(_ sender: Any)  {
        if fieldEmail.text != "" {
            services.authen(email:fieldEmail.text!, delegate: self)
        }
    }
    
    @IBAction func signupWithFacebook (_ sender:Any) {
        loginButtonClicked()
    }
    
    // Once the button is clicked, show the login dialog
    func loginButtonClicked() {
        let loginManager = LoginManager()
        
        loginManager.logIn(readPermissions: [ .publicProfile ], viewController: self) { loginResult in
            switch loginResult {
                case .failed(let error):
                    print(error)
                case .cancelled:
                    print("User cancelled login.")
                case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                    print("Logged in! :", accessToken.authenticationToken)
                    self.services.fbAuthen(fbt: accessToken.authenticationToken, delegate: self)
                
            }
        }

    }

}

extension SigninViewController:ServicesDelegete {
    func callback(from: String, response: Any, tag: String) {
        print("login callback \(response)")
        
        if tag == Services.AUTHENSUCCESS {
            self.navigationController?.popToRootViewController(animated: true)
        } else if tag == Services.AUTHENFAIL {
            let alert = UIAlertController(title: "Warning", message: "Signin fail, please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
}
