//
//  SideMenuTableViewController.swift
//  
//
//  Created by Ch on 14/4/2561 BE.
//

import Foundation
import SideMenu

protocol SideMenuDelegate: class {
    func openPage(_ page: Int)
}

class SideMenuTableViewController: UITableViewController {
    
    @IBOutlet weak var signinCell: UITableViewCell!
    @IBOutlet weak var profileCell: UITableViewCell!
    
    weak var delegate: SideMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        if(User.sharedInstance.id != nil) {
            signinCell.isHidden = true
            profileCell.isHidden = false
        } else {
            signinCell.isHidden = false
            profileCell.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // refresh cell blur effect in case it changed
        tableView.reloadData()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        guard SideMenuManager.default.menuBlurEffectStyle == nil else {
            return
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected!!!! \(indexPath.row)")
        
        dismiss(animated: true, completion: nil)
        
        if indexPath.row == 1 {
            delegate?.openPage(3)
        } else if indexPath.row == 2 {
            delegate?.openPage(1)
        } else if indexPath.row == 3 {
            delegate?.openPage(2)
        }
        
    }
    
}
